module Poomas.Server
  ( module X

  , forkPoomasService
  , PoomasConfig (..)

  , Hubs
  , Services
  , ActiveHubs
  , RefreshHubs (..)
  , refreshHubs

  , MonitorHook
  , MessageHook
  , InfoHook
  )
where

import Poomas.Server.Imports

import Control.Monad.Trans.Except         (ExceptT (..))
import Network.Wai                        (Middleware)
import Network.Wai.Handler.Warp           (defaultSettings, runSettings,
                                           setBeforeMainLoop, setHost, setPort)
import Network.Wai.Middleware.Gzip        (def, gzip)
import Servant.API.Generic                (ToServantApi)


import Poomas.API                         (PoomasConfig (..), hubUri, pCfgLocal)
import Poomas.Server.API                  (AllRoutes, allApi)
import Poomas.Server.Class                (RefreshHubs (..), refreshHubs)
import Poomas.Server.Import.Orphans       ()
import Poomas.Server.Worker.PoomasHubComm as X


-- | Start the application server. It is expected to run the server as a forked
--   process. Server is run in `try` block, so any exception should result in `Left` exit.
forkPoomasService :: PoomasConfig -> IO (PhcEnv, Async ())
forkPoomasService config = do
  (waiApp, app, apMiddleware) <- mkApplication config
  (initPoomasHubComm app,)
    <$> (async . void $ runWaiApp @_ @SomeException app (apMiddleware waiApp))


-- | Run WAI app in `try` block and return `Either` result.
runWaiApp :: (MonadIO m, Exception e) => App -> Application -> m (Either e ())
runWaiApp App{..} app = do
  let settings = setPort (fromIntegral appPort)
               . setHost (fromString $ toS appHost) -- bind only to specific IP
               . setBeforeMainLoop preRunActions
               $ defaultSettings
  liftIO . try $ runSettings    settings app

  where
    preRunActions = say $ "Poomas Hub running on " <> toS appHost <> ":" <> tshow appPort

-- | Create `App` record (holding application state in `Reader`), WAI application
--   and `Middleware`s. If `App` state creation was successful, return created
--   port, `App`, WAI application and mappended list of middlewares.
mkApplication :: PoomasConfig -> IO (Application, App, Middleware)
mkApplication cfg = try (mkFoundation cfg) >>= \case
  Left  (e :: SomeException) -> throwM e
  Right (app, middlewares)   -> pure ( mkAppM poomasApi EmptyContext app allApi
                                     , app
                                     , foldl1 (.) middlewares
                                     )
  where
    poomasApi = Proxy :: Proxy (ToServantApi AllRoutes)
    mkAppM :: forall api a.
              ( HasServer api a
              , HasContextEntry (a .++ DefaultErrorFormatters) ErrorFormatters
              )
           => Proxy api -> Context a -> App -> ServerT api AppM -> Application
    mkAppM api sctx appCfg =
      serveWithContext api sctx . hoistServerWithContext api (Proxy @a) convertApp
      where
        convertApp :: AppM v -> Handler v
        convertApp app = Handler . ExceptT . try $ runRIO appCfg app


-- | Prepare `App` state from given configuration.
--   Prepares a list of possible WAI middlewares:
--     - EKG metrics
--     - gzip payload
--   Caller must start refresher service!
--   Return created `App` and list of `Middleware's.
mkFoundation :: PoomasConfig -> IO (App, [Middleware])
mkFoundation cfg = do
  let BaseUrl{..} = cfg ^. pCfgLocal . hubUri
  (, [gzip def]) <$> do App (toS baseUrlHost)                  -- appHost
                            (fromIntegral baseUrlPort)         -- appPort
                         <$> newManager defaultManagerSettings -- appHttpMan
                         <*> newIORef mempty                   -- appLogFunc
                         <*> newIORef Nothing                  -- appMetric
                         <*> newIORef Nothing                  -- appRefresher
                         <*> newIORef (mkLocalState cfg)       -- appLocalState
