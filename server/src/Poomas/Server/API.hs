module Poomas.Server.API
  ( AllRoutes
  , WaiApp

  , allApi
  )
where

import Poomas.Server.Imports

import Servant.API.Generic          (ToServant)
import Servant.Server.Generic       (AsServerT, genericServerT)


import Poomas.API                   (AllRoutes, Cached (..), CachedValue (..), Hub (..),
                                     KnownHubs, MessageLoad (..), MonitorLoad,
                                     PoomasRoutes (..))
import Poomas.Server.Worker.Refresh (addHubToState, refreshState)


-- | Alias for non-servant, "pure" Wai Application
type WaiApp = AppM Application

-- | Record of 'Handler's for the whole API
allApi :: ToServant PoomasRoutes (AsServerT AppM)
allApi = poomasApi

-- | Record of 'Handler's for Home route
poomasApi :: ToServant PoomasRoutes (AsServerT AppM)
poomasApi = genericServerT PoomasRoutes
  { poomasAliveR     = aliveGetH
  , poomasStateGetR  = poomasStateGetH
  , poomasStatePostR = poomasStatePostH
  , poomasRefreshR   = refreshGetH
  , poomasMonitorR   = monitorPostH
  , poomasMessageR   = messagePostH
  }


-- | If answers, this 'Hub' is alive and kicking
aliveGetH :: AppM Bool
aliveGetH = time'n'Counter "AliveGetRoute" $ do
  logInfoHS "Poomas" "alive" >> return True

-- | Refresh state and return known state in Cached form
refreshGetH :: AppM (Cached KnownHubs)
refreshGetH = time'n'Counter "RefreshGetRoute" $ do
  logInfoHS "Poomas" "refresh"
  refreshState >> getCachedHubs


-- | Insert 'Hub' to current state and return known state in Cached form
poomasStatePostH :: Cached Hub -> AppM (Cached KnownHubs)
poomasStatePostH (fromCached -> p) = time'n'Counter "StatePostRoute" $ do
  logInfoHS "Poomas" "post state"
  either (\e -> throwM err422{errBody = toS $ show p <> " :: " <> e}) addHubToState p

-- | Return known state in Cached form
poomasStateGetH :: AppM (Cached KnownHubs)
poomasStateGetH = time'n'Counter "StateGetRoute" $ do
  logInfoHS "Poomas" "get state" >> getCachedHubs


-- curl -i -POST localhost:3015/monitor --header "Content-Type: application/json" -d '{"mlEvent":"pong", "mlFrom":"meme","mlHubs":[], "mlMessage":"MYmessage"}'

-- | Receive a 'Monitor' event
monitorPostH :: MonitorLoad -> AppM NoContent
monitorPostH ml = time'n'Counter "MonitorPostRoute" $ do
  logInfoHS "Poomas" "monitor"
  view lsMonitorHook <$> getLocalState >>= liftIO . maybe (pure ()) ($ ml)
  return NoContent

-- | Receive a direct message
messagePostH :: MessageLoad -> AppM NoContent
messagePostH ml = time'n'Counter "MessagePostRoute" $ do
  logInfoHS "Poomas" "message"
  view lsMessageHook <$> getLocalState >>= liftIO . maybe (pure ()) ($ ml)
  return NoContent
