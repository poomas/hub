{-# OPTIONS_GHC -fno-warn-orphans #-}
module Poomas.Server.Worker.Instances where

import Poomas.Server.Imports

import Poomas.API            (ActiveHub (..), CachedValue (..), HubDataState (..),
                              HubReqs (..), HubUri (..), KnownHubs, ahHub, ahPrepedReq,
                              hubDataState, hubUri, mkActiveHub, pCfgRefresh,
                              srRetryDelaySec)
import Poomas.Server.Class   (RefreshHubs (..))


-- | Instance for fetching hub Poomas state in 'AppM' monad
instance RefreshHubs AppM where
  -- ^ Use prepared client functions from 'ActiveHub'
  refreshHubAlive h = do
    App{..} <- ask
    now     <- liftIO getCurrentTime
    liftIO . prAlive . view ahPrepedReq $ mkActiveHub appHttpMan now h

  -- ^ Make the specified 'Hub' an 'ActiveHub' and make request
  refreshHubFetch h = do
    App{..} <- ask
    now     <- liftIO getCurrentTime
    locS    <- getLocalState
    let repeatAfter = locS ^. lsPoomasCfg . pCfgRefresh . srRetryDelaySec
    if h ^. hubDataState == HdsCurrent
      then pure $ Right (h & hubDataState .~ mempty, mempty)
      else either (const (Left h))
                  (Right . second (mapFromList . map (view hubUri &&& id)))
              <$> getHubState repeatAfter (mkActiveHub appHttpMan now h)


-- | Fetch Poomas state from an `ActiveHub` and return `Hubs`
--   If given a Just repeatAfter and first call fails, it will be repeated
--   only once after specified delay.
--   Delay can be set in configuration and changed dynamically
getHubState :: Maybe Word -> ActiveHub -> AppM (Either String KnownHubs)
getHubState repeatAfter ah = time'n'Counter "Fetch.Hub" $ do
  logDebugHS "Poomas" $ "Fetch hub state from " <> toS (showBaseUrl $ ah ^. ahHub . hubUri)
  myself <- toCached <$> getMyself
  let getState = (prPoomasStatePost $ ah ^. ahPrepedReq) myself

  s <- liftIO $ either (\e -> maybe (pure $ Left e)
                                    (\t -> threadDelay (fromIntegral t) >> getState)
                                     repeatAfter
                       )
                       (pure . Right)
                  =<<  getState
  case s of
    Right aps -> logDebugHS "Poomas" ("Success: " <> hubBurl) >> pure (Right aps)
    Left e    -> do
      logWarnHS  "Poomas" ("GetHubState failed: "  <> hubBurl <> " :: " <> toS e)
      pure (Left $ toS e)

  where
    hubBurl :: ScribeMsg
    hubBurl = toS (showBaseUrl $ getHubUri (ah ^. ahHub))
