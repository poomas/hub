{-# OPTIONS_GHC -fno-warn-orphans #-}
module Poomas.Server.Worker.Refresh where

import Poomas.Server.Imports

import Poomas.API                        (Cached (..), CachedValue (..), Hub (..),
                                          HubDataState (..), KnownHubs, ahHub, hubNeed,
                                          hubUri, mkActiveHub, needInfo, needMonitor,
                                          needService, pCfgBeacons, pCfgRefresh,
                                          srHeartbeatSec, srMaxLevels)
import Poomas.Server.Class               (refreshHubs)
import Poomas.Server.Worker.Extractors   (info4Master, monitors4Master, services4Master)
import Poomas.Server.Worker.Instances    ()
import Poomas.Server.Worker.Notification

-- | Main function for refreshing the known swarm state
--   - fetch the new swarm state from network
--   - log and notify 'Monitor' about missing 'Hub's
--   - replace the old state with new
refreshState :: AppM ()
refreshState = time'n'Counter "Refresh.State" $ do
  logInfoHS "Poomas" "Refreshing state"
  myself           <- getMyself
  (currAps, swCfg) <- (view lsActiveHubs &&& view lsPoomasCfg) <$> getLocalState

  let beacons = swCfg ^. pCfgBeacons
      fltMiss = filter (`notElem` beacons) . map (view hubUri) . toList
  (hubs, fltMiss -> miss) <- refreshHubs myself (swCfg ^. pCfgRefresh . srMaxLevels)
                          .  nub $ beacons <> keys currAps

  unless (null miss) $ do
    void $ sendPoomasEvent (map showBUrl miss) "Missing"
    logWarnHS "Poomas" $ "Missing hubs: " <> concatMap showBUrl miss

  void $ setHubsToState hubs

-- | Add new Hub to current known swarm state and return current known swarm state
--   in Cached form
addHubToState :: Hub -> AppM (Cached KnownHubs)
addHubToState (setHubState HdsCurrent -> h) = do
  currHubs <- map (view ahHub) . view lsActiveHubs <$> getLocalState
  if lookup (h ^. hubUri) currHubs == Just h
    then modifyLocalState updateHub >> getCachedHubs
    else setHubsToState $ insertHub h currHubs
 where
   updateHub = (,()) . (& lsActiveHubs %~ adjustMap (& ahHub .~ h) (h ^. hubUri))

-- | Replace old known swarm state with new one
--   - if new state is the same as the old one, do nothing
--   - if change occurred, log it and notify 'Monitor'
--   - return current known swarm state in Cached form
setHubsToState :: Hubs -> AppM (Cached KnownHubs)
setHubsToState hs = do
  App{..}  <- ask
  now      <- liftIO getCurrentTime
  myself   <- getMyself
  currHubs <- map (view ahHub) . view lsActiveHubs <$> getLocalState
  let newHubs = filterMap (/= myself) hs
  if newHubs == currHubs
    then getCachedHubs
    else do
      old <- getLists <$> getLocalState
      modifyLocalState (updateLocalState now appHttpMan newHubs)
      notifyChange old . getLists =<< getLocalState
      getCachedHubs

  where
    getLists locS = (locS ^. lsServices, locS ^. lsInfo, locS ^. lsMonitors)

-- | Main function for updating data in 'LocalState'
--   Does all the calculations, modifications and creations:
--     -
updateLocalState :: UTCTime -> Manager -> Map BaseUrl Hub -> LocalState -> (LocalState, ())
updateLocalState now man newHubs locS = do
  let myself = locS ^. lsMyself
      needs  = myself ^. hubNeed

  ( locS & lsActiveHubs .~ map (mkActiveHub man now) newHubs
         & lsJsonCached .~ toCached (myself, toList newHubs)
         & lsServices   .~ services4Master     (needs ^. needService) newHubs
         & lsInfo       .~ info4Master         (needs ^. needInfo)    newHubs
         & lsMonitors   .~ monitors4Master man (needs ^. needMonitor) newHubs
    , ())


-- | Recreate cached objects and store it in application state
reCache :: LocalState -> LocalState
reCache locS = do
  locS & lsJsonCached .~ toCached ( locS ^. lsMyself
                                  , toList $ map (view ahHub) $ locS ^. lsActiveHubs
                                  )


------------------------------------------------------------------------------------------
-- Background process for refreshing state
------------------------------------------------------------------------------------------

-- | Background refresh sub-process.
--   - Forked from main Poomas thread.
--   - Sleeps for the seconds amount defined in configuration
--   - Always reads current refresh value, so can be changed dynamically
refreshProcess :: (MonadIO m) => App -> m ()
refreshProcess app = do
  liftIO $ do
    runRIO app refreshState
    rfrshCfg <- view (lsPoomasCfg . pCfgRefresh) <$> liftIO (readIORef $ appLocalState app)
    threadDelay . round  $ 1_000_000 * rfrshCfg ^. srHeartbeatSec
  refreshProcess app


-- | Stop refresher service, only if active
stopRefresher :: MonadIO m => App -> m ()
stopRefresher App{..} = readIORef appRefresher >>= \case
  Nothing -> pure ()
  Just th -> writeIORef appRefresher Nothing >> uninterruptibleCancel th


-- | Stop current refresher service, if active, and start a new one
startRefresher :: MonadUnliftIO m => App -> m ()
startRefresher app@App{..} = stopRefresher app
                         >> (writeIORef appRefresher
                          .  Just
                        =<<  async (refreshProcess app))
