module Poomas.Server.Worker.Extractors
  ( services4Master
  , info4Master
  , monitors4Master

  , byOfferedInfo
  , byOfferedMonitor
  , byOfferedService
  , byNeededService

  )
where

import Poomas.Server.Imports

import RIO.List              (nubBy)
import RIO.Map               (filterWithKey, fromListWith)
import Servant.Client        (mkClientEnv, runClientM)


import Poomas.API            (Hub (..), Monitor (..), EventType (..),
                              OfferedService (..), Service (..), monitorC, needService,
                              offerInfo, offerMonitor, offerService)


-- | Extract only needed services from all offered services
services4Master :: Set Service -> Hubs -> Services
services4Master ss = filterWithKey (\k _ -> k `member` ss) . byOfferedService

-- | Extract only needed info from all offered info
info4Master :: Set Text -> Hubs -> Infos
info4Master ts = filterWithKey (\k _ -> k `member` ts) . byOfferedInfo

-- | Extract only monitors specified in config as a need
--   Return:
--    - If set is empty, no Monitors
--    - If set has name "*", all Monitors
--    - Otherwise all from given set
monitors4Master :: Manager -> Set Monitor -> Hubs -> Monitors
monitors4Master man (toList -> ms) hubs =
  map (map mkMonFuncs)
    $ case map monitorName ms of
        []    -> mempty
        ["*"] -> offeredMons
        ns    -> map (filter (flip elem ns . monitorName . fst)) offeredMons

  where
    offeredMons        = byOfferedMonitor hubs
    mkMonitorC cEnv ml = runClientM (monitorC ml) cEnv
    mkMonFuncs         = second (mkMonitorC . mkClientEnv man)


-- | Create a map of offered 'Service's with corresponding 'Hub' providers (by version)
byOfferedService :: Hubs -> Services
byOfferedService = foldr go mempty
  where
    mrg (a1,_) (a2,_)         = (nub . sort $ a1 <> a2, Nothing)
    conv (OfferedService s u) = (s, ([u], Nothing))
    go (Hub _ _ offer _) acc  = unionWith mrg acc
                              . fromListWith mrg
                              . map conv
                              $ toList (offer ^. offerService)


-- | Create a map of needed 'Service's with corresponding 'Hub' providers (by version)
byNeededService :: Hubs -> Services
byNeededService = foldr go mempty
  where
    mrg (a1,_) (a2,_)       = (nub . sort $ a1 <> a2, Nothing)
    go (Hub b need _ _) acc = unionWith mrg acc
                            . fromListWith mrg
                            . map (, ([b], Nothing))
                            $ toList (need ^. needService)


-- | Create a map of offered 'Info' with corresponding 'Hub' providers
byOfferedInfo :: Hubs -> Infos
byOfferedInfo = map groupByFirst . fromListWith (<>) . foldr go mempty
  where
    conv b                   = map (\(k,v) -> (k, [(v,b)])) . toList
    go (Hub b _ offer _) acc = acc <> conv b (offer ^. offerInfo)


-- | Create a map of offered 'Monitor's with corresponding 'Hub' providers
byOfferedMonitor :: Hubs -> Map EventType [(Monitor, BaseUrl)]
byOfferedMonitor = map (nubBy ((==) `on` snd)) . foldr go mempty
  where
    go (Hub b _ offer _) acc = do
      unionWith (<>) acc . fromListWith (<>) $ concatMap conv (offer ^. offerMonitor)
      where
        conv m = map (,[(m,b)]) $ monitorEvent m


-- | Group tuples by the first arg
groupByFirst :: Ord a => [(a, BaseUrl)] -> [(a, [BaseUrl])]
groupByFirst (sort -> xs) = mapMaybe conv $ groupBy ((==) `on` fst) xs
  where conv ys = (, map snd xs) . fst <$> headMay ys
