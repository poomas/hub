module Poomas.Server.Worker.PoomasHubComm
  ( PhcEnv
  , initPoomasHubComm

  -- Information
  , getSwarmState
  , getOfferedInfo
  , getBaseUrl

  -- Notifications
  , sendSwarmEvent
  , sendLog
  , sendException
  , sendTrack
  , sendMessage

  -- Configuration
  , setLoadBalancer
  , setHeartbeatSec
  , setInfo
  , addInfo
  , setEKGStore
  , setLogger
  , addInitHub
  , removeInitHub
  -- , removeOffer
  -- , addOffer

  -- Hooks
  , setInfoHook
  , setMonitorHook
  , setMessageHook

  -- Control
  , stopHubRefresher
  , startHubRefresher
  )
where

import           Poomas.Server.Imports

import           Data.UUID                         (UUID)
import qualified RIO.List                          as L
import qualified RIO.Set                           as S

import           Poomas.API                        (CachedValue (..), Hub (..), KnownHubs,
                                                    LoadBalancerType (..),
                                                    MessageLoad (..), Service (..), ahHub,
                                                    ahPrepedReq, hubOffer, hubUri,
                                                    offerInfo, offerService,
                                                    offerSrvService, pCfgBeacons,
                                                    pCfgRefresh, prMessagePost,
                                                    serviceName, srHeartbeatSec)
import           Poomas.Server.Import.LoadBalancer (lBalancerInit, lBalancerNext)
import           Poomas.Server.Worker.Notification (sendPoomasEvent, sendPoomasException,
                                                    sendPoomasLog, sendPoomasTrack)
import           Poomas.Server.Worker.Refresh      (reCache, startRefresher,
                                                    stopRefresher)

------------------------------------------------------------------------------------------
-- PoomasHubComm implementations
------------------------------------------------------------------------------------------

newtype PhcEnv = PhcEnv {unPhcEnv :: App}

-- | Create 'PhcEnv' used to communicate with hub service
initPoomasHubComm :: App -> PhcEnv
initPoomasHubComm = PhcEnv


-- | Main master/poomas service communication protocol. Any data returned is the data
--   before the function was applied, i.e. previous state.

------------------------------------------------------------------------------------------
-- Control
------------------------------------------------------------------------------------------

-- | Kill the thread of refresher sub-process
stopHubRefresher :: MonadIO m => PhcEnv -> m ()
stopHubRefresher = stopRefresher . unPhcEnv

-- | Start new refresher sub-process. The old one is killed, if exists
startHubRefresher :: MonadUnliftIO m => PhcEnv -> m ()
startHubRefresher = startRefresher . unPhcEnv


------------------------------------------------------------------------------------------
-- Information
------------------------------------------------------------------------------------------

-- | Get current swarm state
getSwarmState :: PhcEnv -> IO (Either String KnownHubs)
getSwarmState (PhcEnv app) = runRIO app $ fromCached <$> getCachedHubs

-- | Get the Info from the swarm. Return all the answers, grouped by answer
getOfferedInfo :: PhcEnv -> Text -> IO [(Text, [BaseUrl])]
getOfferedInfo (PhcEnv app) tag = runRIO app $ do
  infos <- view lsInfo <$> getLocalState
  pure . maybeToList $ headMay =<< lookup tag infos

-- | Get the BaseUrl of the service, load-balance if more providers
getBaseUrl :: PhcEnv -> Service -> IO (Maybe BaseUrl)
getBaseUrl (PhcEnv app) s@(Service _ _ st) = runRIO app $
  join <$> (traverse scheduler . lookup s =<< view lsServices <$> getLocalState)
  where
    scheduler (xs, sch) = lBalancerNext =<< maybe (lBalancerInit st xs) pure sch

------------------------------------------------------------------------------------------
-- Notifying Monitors
------------------------------------------------------------------------------------------

-- | Send an alert to swarm. Only `Monitor`s listen to alerts
sendSwarmEvent :: PhcEnv -> Text -> IO Int
sendSwarmEvent (PhcEnv app) msg = runRIO app $ sendPoomasEvent [] msg

-- | Send an alert to swarm. Only `Monitor`s listen to alerts
sendLog :: PhcEnv -> Text -> Text -> IO Int
sendLog (PhcEnv app) org msg = runRIO app $ sendPoomasLog org msg

-- | Send an alert to swarm. Only `Monitor`s listen to alerts
sendException :: PhcEnv -> Text -> Text -> IO Int
sendException (PhcEnv app) org msg = runRIO app $ sendPoomasException org msg

-- | Send an alert to swarm. Only `Monitor`s listen to alerts
sendTrack :: PhcEnv -> UUID -> Text -> IO Int
sendTrack (PhcEnv app) org msg = runRIO app $ sendPoomasTrack org msg


-- | Send direct message to instances by BaseUrl or Services by their name
sendMessage :: PhcEnv -> [Either BaseUrl Text] -> MessageLoad -> IO Int
sendMessage (PhcEnv app) rcps ml = do
  ahs <- runRIO app $ mapToList . view lsActiveHubs <$> getLocalState
  let recipients = case rcps of
                     [] -> ahs
                     rs -> do
                       let (burls, srvcs) = partitionEithers rs
                       filter (flt (burls, srvcs)) ahs
  fmap (length . rights) . mapM ($ ml) $ msgClients recipients

  where
    flt (burls, srvcs) (burl, ah) = if
      | burl `elem` burls                             -> True
      | srvcs `L.intersect` extractSrvcNames ah /= [] -> True
      | otherwise                                     -> False

    extractSrvcNames ah = toList . S.map (^. offerSrvService . serviceName)
                                 $ ah ^. ahHub . hubOffer . offerService
    msgClients = map (prMessagePost . view ahPrepedReq . snd)


------------------------------------------------------------------------------------------
-- Configuration
------------------------------------------------------------------------------------------

-- | Change Scheduler for the Service
setLoadBalancer :: PhcEnv -> Service -> LoadBalancerType -> IO ()
setLoadBalancer (PhcEnv app) s st = runRIO app . modifyLocalState $ (,())
  . (& lsServices %~ replaceKey)

  where
    replaceKey m = maybe m (flip (insertMap s{_serviceLBType=st}) (deleteMap s m))
                     $ lookup s m

-- | Set new heartbeat seconds
setHeartbeatSec :: PhcEnv -> Float -> IO Float
setHeartbeatSec (PhcEnv app) secs = runRIO app . modifyLocalState $
      (&  lsPoomasCfg . pCfgRefresh . srHeartbeatSec .~ secs)
  &&& (^. lsPoomasCfg . pCfgRefresh . srHeartbeatSec)


-- | Replace the whole Offered Info with this new list
setInfo :: PhcEnv -> Set (Text, Text) -> IO ()
setInfo (PhcEnv app) is = runRIO app . modifyLocalState $ (,())
  . reCache . (& lsMyself . hubOffer . offerInfo .~ is)

addInfo :: PhcEnv -> (Text, Text) -> IO ()
addInfo (PhcEnv app) info = runRIO app . modifyLocalState $ (,())
  . reCache . (& lsMyself . hubOffer . offerInfo %~ S.insert info)


-- | Set new or remove the old EKG store
setEKGStore :: PhcEnv -> Maybe Store -> IO ()
setEKGStore (PhcEnv app@App{..}) store =
  runRIO app . writeIORef appMetric =<< initializeWith `traverse` store


-- | Set new logging function. For removing, use "return ()"
setLogger :: PhcEnv -> ScribeLog -> IO ()
setLogger (PhcEnv app@App{..}) = runRIO app . writeIORef appLogFunc


-- | Add a URL to the list of initial Hub URLs list
addInitHub :: PhcEnv -> Hub -> IO [BaseUrl]
addInitHub (PhcEnv app) (view hubUri -> bUrl) = runRIO app . modifyLocalState $
      (&  lsPoomasCfg . pCfgBeacons %~ nub . cons bUrl)
  &&& (^. lsPoomasCfg . pCfgBeacons)


-- | Remove a URL from the list of initial Hub URLs list
removeInitHub :: PhcEnv -> Hub -> IO [BaseUrl]
removeInitHub (PhcEnv app) (view hubUri -> bUrl) = runRIO app . modifyLocalState $
      (&  lsPoomasCfg . pCfgBeacons %~ filter (/= bUrl))
  &&& (^. lsPoomasCfg . pCfgBeacons)


-- removeOffer :: PhcEnv -> Offer -> IO (Set Offer)
-- removeOffer (PhcEnv app@App{..}) o = runRIO app . modifyLocalState $
--       (&  lsMyself . hubOffer %~ S.filter (/= o))
--   &&& (^. lsMyself . hubOffer)

-- addOffer :: PhcEnv -> Offer -> IO Offer
-- addOffer (PhcEnv app@App{..}) o = runRIO app $ do
--   modifyLocalState  ((&  lsMyself . hubOffer %~ insertSet o)
--                  &&& (^. lsMyself . hubOffer))

------------------------------------------------------------------------------------------
-- Hooks
------------------------------------------------------------------------------------------

-- | Set or remove the hook for receiving notifications about changed Info in the swarm
setInfoHook :: PhcEnv -> Maybe InfoHook -> IO ()
setInfoHook (PhcEnv app) ih = runRIO app . modifyLocalState $ (,())
  . (& lsInfoHook .~ ih)

-- | Set or remove the hook for receiving Monitor notifications
setMonitorHook :: PhcEnv -> Maybe MonitorHook -> IO ()
setMonitorHook (PhcEnv app) mh = runRIO app . modifyLocalState $ (,())
  . (& lsMonitorHook .~ mh)

-- | Set or remove the hook for receiving direct messages
setMessageHook :: PhcEnv -> Maybe MessageHook -> IO ()
setMessageHook (PhcEnv app) mh = runRIO app . modifyLocalState $ (,())
  . (& lsMessageHook .~ mh)
