module Poomas.Server.Worker.Notification where

import Poomas.Server.Imports

import Data.UUID             (UUID)

import Poomas.API            (EventData (..), EventType (..), MonitorLoad (..),
                              PoomasEventLoad (..), PoomasExceptionLoad (..),
                              PoomasLogLoad (..), PoomasTrackLoad (..), getEventDataType,
                              hubUri, monitorName)



-- | Notify about changes in services, info or monitors
notifyChange :: (Services,Infos,Monitors) -> (Services,Infos,Monitors) -> AppM ()
notifyChange (oldSrv, oldInfo, oldMons) (newSrv, newInfo, newMons) = do
  when (oldInfo /= newInfo) notifyInfo
  when (oldSrv /= newSrv) $ do
    let payload = nub . sort . concatMap (map showBUrl . fst) $ toList newSrv
    void $ sendPoomasEvent payload "Service"
  when (oldMonsPrepd /= newMonsPrepd) $
    void $ sendPoomasEvent (nub . sort . concat $ toList newMonsPrepd) "Monitor"

  out 0 . ((<>) "Change: " . showActiveHubs) . view lsActiveHubs =<< getLocalState

  where
    oldMonsPrepd = removeMonNotify oldMons
    newMonsPrepd = removeMonNotify newMons
    removeMonNotify :: Monitors -> Map EventType [Text]
    removeMonNotify = map (nub . map (monitorName . fst))

-- | Notify 'Master' about the possible 'Info' change, if hook is set
notifyInfo :: AppM ()
notifyInfo = do
  locS <- getLocalState
  liftIO $ maybe (pure ()) ($ mapToList $ locS ^. lsInfo) $ locS ^. lsInfoHook

-- | Notify all 'Monitors', if any exists, about a PoomasEvent
sendPoomasEvent :: (HasLocalState m, MonadIO m) => [Text] -> Text ->  m Int
sendPoomasEvent ps  = sendMonitorLoad . EventPoomas . PoomasEventLoad ps

-- | Send log data to all 'Monitors', if any exists
sendPoomasLog :: (HasLocalState m, MonadIO m) => Text -> Text ->  m Int
sendPoomasLog origin  = sendMonitorLoad . EventLog . PoomasLogLoad origin

-- | Send data about unexpected exception to all 'Monitors', if any exists
sendPoomasException :: (HasLocalState m, MonadIO m) => Text -> Text ->  m Int
sendPoomasException origin = sendMonitorLoad . EventException . PoomasExceptionLoad origin

-- | Notify all 'Monitors', if any exists, about a tracking event
sendPoomasTrack :: (HasLocalState m, MonadIO m) => UUID -> Text ->  m Int
sendPoomasTrack uuid  = sendMonitorLoad . EventTrack . PoomasTrackLoad uuid


-- | Notify all 'Monitor' of some change, if any exists
sendMonitorLoad :: (HasLocalState m, MonadIO m) => EventData ->  m Int
sendMonitorLoad md = do
  locS   <- getLocalState
  myself <- getMyself

  let monitors = fromMaybe [] $ lookup (getEventDataType md) (locS ^. lsMonitors)
      mLoad    = MonitorLoad (myself ^. hubUri) 0 md

  liftIO $ do
    maybe (pure ()) ($ mLoad) $ locS ^. lsMonitorHook
    sendData mLoad $ map snd monitors

  where
    sendData _ []                                 = pure 0
    sendData mLoad@MonitorLoad{..} (monitor:rest) = do
      let ml = mLoad{mlRcptOrder = mlRcptOrder + 1}
      liftIO $ monitor ml >>= \case
        Right _ -> (+) 1 <$> sendData ml rest
        Left e  -> do
          say $ "SendPoomasEvent error: " <> tshow e
          pure 0
