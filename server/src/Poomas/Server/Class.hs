module Poomas.Server.Class where

import Poomas.Server.Imports

import RIO.Map               (differenceWith)

import Poomas.API            (Hub (..), HubUri (..), hubDataState, hubUri)


-- | Class of functions working in some  Monad
class RefreshHubs m where
  -- ^ Check if specified 'Hub' is alive. Return False on any error as well
  refreshHubAlive :: Monad m => Hub  -> m Bool

  -- ^ Fetch the Poomas state from the specified 'Hub'.
  --   On error, return the 'Hub' it was called for as Left, so it can be recorded a miss.
  refreshHubFetch :: Monad m => Hub  -> m (Either Hub (Hub, Hubs))

  -- ^ Fetch the Poomas state for specified 'Hub's ("originals").
  --   Return a triple with three lists consisting of
  --     - missing 'Hub's
  --     - fetched info from the "original" 'Hub's
  --     - info from the first children of the "original" 'Hub's
  --     - based on hubDataState, can skip re-fetching since data is current
  refreshHubChildren :: Monad m => Hubs -> m (Hubs, Hubs, Hubs)

  -- ^ default implementations
  refreshHubAlive    = const (pure False)
  refreshHubFetch    = pure . Left
  refreshHubChildren = foldrM go mempty
      where
      go hub (miss, hs, cs) =
        (\case Left   h    -> (insertHub h miss,             hs,                      cs)
               Right (h,m) -> (            miss, insertHub h hs, unionWith mergeHub m cs)
        ) <$> refreshHubFetch hub
  {-# MINIMAL refreshHubFetch | refreshHubChildren #-}


-- | Main refreshing algorithm
--   - skip checking myself
--   - fetch info from parents and their children
--   - loop maxLevels deep into children's children
--   - keep track of already visited 'Hub's and skip repeating request
--   - mark all new 'Hub' data state as Old, so it will be refreshed next time
--   - more optimisations can be added in this place, like:
--      - should just accept dead 'Hub's from children or recheck?
--      - take care not to produce more requests than without optimisation
refreshHubs :: (HubUri a, RefreshHubs m, Monad m) => Hub -> Word8 -> [a] -> m (Hubs, Hubs)
refreshHubs myself maxLevels bUrls =
  first (map (& hubDataState .~ mempty) . deleteMap (myself ^. hubUri))
    <$> loop initAps (singletonMap (myself ^. hubUri) myself, mempty) maxLevels
  where
    loop !aps (done, prevMiss) !level =
      if level <= 0 || null aps
        then pure (done, prevMiss)
        else do
          let (!newActive, !newDone, !sepMiss) = separate aps done prevMiss
          (!newMiss, !parents, !children)  <- refreshHubChildren newActive
          loop children
              (unite newDone parents, unite prevMiss (unite newMiss sepMiss))
              (level-1)

    unite   = unionWith mergeHub
    initAps = mapFromList $ map (\(getHubUri -> b) -> (b, Hub b mempty mempty mempty))
                                bUrls
    separate aps done miss =
      let diff = aps \\ done
       in ( diff \\ miss                                         -- new
          , unite aps done \\ diff                               -- done
          , differenceWith (\p1 -> Just . mergeHub p1) miss diff -- miss
          )
