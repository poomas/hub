{-# LANGUAGE TemplateHaskell #-}
module Poomas.Server.App where

import Poomas.Server.Import.CommonImports

import Poomas.API                         (ActiveHub, Cached (..), EventType, Hub,
                                           KnownHubs, LoadBalancerType (..),
                                           MessageLoad (..), Monitor (..), MonitorLoad,
                                           MonitorNotify, PoomasConfig (..), Service,
                                           pCfgLocal, toCached)

-- | Application with `App` as the `Reader` state
type AppM = RIO App

-- | Container of 'Hub's by their 'BaseUrl'
type Hubs = Map BaseUrl Hub

-- | Container of 'Service' providers by the provided 'Service'
type Services = Map Service ([BaseUrl], Maybe LoadBalancer)

-- | Container of 'Hub's that are found alive in the swarm
type ActiveHubs = Map BaseUrl ActiveHub

-- | Container of 'Monitor's found in the swarm
type Monitors = Map EventType [(Monitor, MonitorNotify)]

-- | Container of 'Info' data found in the swarm
type Infos = Map Text [(Text, [BaseUrl])]

-- | Hook for receiving 'Monitor' notifications from the swarm
type MonitorHook = MonitorLoad -> IO ()

-- | Hook for receiving direct messages
type MessageHook = MessageLoad -> IO ()

-- | Hook for receiving notifications about 'Info data changed in the swarm
type InfoHook = [(Text,[(Text,[BaseUrl])])] -> IO ()


-- | LoadBalancerType for tween services
data LoadBalancer = LoadBalancer
  { lbLBType :: LoadBalancerType -- ^ the current LoadBalancerType type
  , lbRef    :: IORef [BaseUrl]  -- ^ container of URLs (from Twin services)
  }
instance Show LoadBalancer where show = show . lbLBType
instance Eq   LoadBalancer where a == b = lbLBType a == lbLBType b


-- | Application state, both static and dynamic parts
data App = App
  { --  Static
    appHost       :: Text                     -- ^ http/s address
  , appPort       :: Word                     -- ^ port on which the app is run
  , appHttpMan    :: Manager                  -- ^ http manager

    --  Dynamic
  , appLogFunc    :: IORef  ScribeLog         -- ^ logging function
  , appMetric     :: IORef (Maybe Metrics)    -- ^ metrics for EKG
  , appRefresher  :: IORef (Maybe (Async ())) -- ^ refresher sub-process thread
  , appLocalState :: IORef  LocalState        -- ^ local state of the swarm and Poomas cfg

  }

-- | Class of functions for getting and atomically modifying the local Poomas state
class Monad m => HasLocalState m where
  getLocalState    :: m LocalState
  modifyLocalState :: (LocalState -> (LocalState,a)) -> m a

-- | Instance of HasLocalState for AppM monad
instance HasLocalState AppM where
  getLocalState      = liftIO . readIORef                 =<< asks appLocalState
  modifyLocalState f = liftIO . flip atomicModifyIORef' f =<< asks appLocalState


-- | Local state. Holds all the information needed to function
data LocalState = LocalState
  { _lsActiveHubs  :: !ActiveHubs          -- ^ container of Hubs alive in the swarm
  , _lsJsonCached  :: !(Cached KnownHubs)  -- ^ cached representation of known hubs
  , _lsServices    :: !Services            -- ^ list of Services offered by the swarm
  , _lsMonitors    :: !Monitors            -- ^ list of Monitors offered by the swarm
  , _lsMonitorHook :: !(Maybe MonitorHook) -- ^ Masters hook for getting Monitor events
  , _lsMessageHook :: !(Maybe MessageHook) -- ^ Masters hook for receiving direct msgs
  , _lsInfoHook    :: !(Maybe InfoHook)    -- ^ Masters hook for receiving Info changes
  , _lsInfo        :: !Infos               -- ^ container of Info offered by the swarm
  , _lsMyself      :: !Hub                 -- ^ configuration of this Hub
  , _lsPoomasCfg   :: !PoomasConfig        -- ^ swarming configuration
  } deriving (Generic)

-- | Create empty 'LocalState' from given Poomas configuration
mkLocalState :: PoomasConfig -> LocalState
mkLocalState cfg = LocalState
  { _lsActiveHubs  = mempty
  , _lsJsonCached  = toCached (myself, [])
  , _lsServices    = mempty
  , _lsMonitors    = mempty
  , _lsMonitorHook = mempty
  , _lsMessageHook = mempty
  , _lsInfoHook    = mempty
  , _lsInfo        = mempty
  , _lsMyself      = myself
  , _lsPoomasCfg   = cfg
  }
  where
    myself = cfg ^. pCfgLocal

makeLenses ''LocalState
