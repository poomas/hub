{-# OPTIONS_GHC -fno-warn-orphans #-}
module Poomas.Server.Import.Orphans where

import RIO

import RIO.Orphans    ()
import Servant.Client (BaseUrl (..), Scheme (..))



instance Hashable Scheme
instance Hashable BaseUrl
