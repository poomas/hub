module Poomas.Server.Import.Logger where

import Poomas.Server.Import.CommonImports

import Poomas.Server.App


-- | Log a message
logDebugH, logInfoH, logWarnH, logErrorH :: ScribeMsg -> AppM ()
logDebugH = logDebugHS ""
logInfoH  = logInfoHS  ""
logWarnH  = logWarnHS  ""
logErrorH = logErrorHS ""

-- | Log a message with source location
logDebugHS, logInfoHS, logWarnHS, logErrorHS :: ScribeSource -> ScribeMsg -> AppM ()
logDebugHS = doLogH LevelDebug
logInfoHS  = doLogH LevelInfo
logWarnHS  = doLogH LevelWarn
logErrorHS = doLogH LevelError

-- | Log a message with custom level
logOtherH :: Text -> Text -> AppM ()
logOtherH = logOtherHS ""

-- | Log a message with source location with custom level
logOtherHS :: Text -> ScribeSource -> ScribeMsg -> AppM ()
logOtherHS = doLogH . LevelOther


-- | Main logging function aware of the environment
doLogH :: LogLevel -> ScribeSource -> ScribeMsg -> AppM ()
doLogH lvl src msg = do
  f <- liftIO . readIORef =<< asks appLogFunc
  liftIO $ f src lvl msg
