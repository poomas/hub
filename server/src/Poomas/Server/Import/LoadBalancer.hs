module Poomas.Server.Import.LoadBalancer
  ( LoadBalancer
  , LoadBalancerType (..)

  , lBalancerInit
  , lBalancerSetItems
  , lBalancerNext
  )
where

import Poomas.Server.Imports hiding (head)

import Data.List             (cycle)

import Poomas.API            (LoadBalancerType (..))


-- | Initialise LoadBalancer
lBalancerInit :: MonadIO m => LoadBalancerType -> [BaseUrl] -> m LoadBalancer
lBalancerInit t xs = LoadBalancer t <$> newIORef (cycle $ toList xs)

-- | Replace Scheduler items
lBalancerSetItems :: MonadIO m => LoadBalancer -> [BaseUrl] -> m ()
lBalancerSetItems (lbRef -> r) xs = writeIORef r (cycle $ toList xs)

-- | Fetch next item from LoadBalancerType's list
lBalancerNext :: MonadIO m => LoadBalancer -> m (Maybe BaseUrl)
lBalancerNext LoadBalancer{..} = atomicModifyIORef lbRef $ \case
    []       -> ([], Nothing)
    (a : as) -> case lbLBType of
      LbtRoundRobin  -> (    as, Just a)
      LbtAlwaysFirst -> (a : as, Just a)
