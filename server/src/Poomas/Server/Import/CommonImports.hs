module Poomas.Server.Import.CommonImports
  ( module X

  , ScribeMsg
  , ScribeSource
  , ScribeLog
)
where

import ClassyPrelude             as X hiding (Handler)
import RIO                       as X (MonadThrow, RIO (..), ThreadId, display,
                                       displayShow, local, logOptionsHandle, runRIO,
                                       threadDelay, throwM)

import Control.Arrow             as X ((+++), (|||))
import Control.Error             as X (failWith, hoistEither, hush, note, (??))
import Control.Exception.Safe    as X (MonadMask)
import Control.Monad.Logger      as X (LogLevel (..))
import Control.Monad.Metrics     as X
import Data.Aeson                as X
import Data.Aeson.Types          as X (Pair)
import Data.Char                 as X hiding (toLower, toUpper)
import Data.EitherR              as X
import Data.Foldable             as X (foldl, foldl1, foldr1, foldrM)
import Data.List                 as X (nub)
import Data.String.Conv          as X
import Debug                     as X
import Lens.Micro.Platform       as X hiding (at, (.=))
import Network.HTTP.Client       as X hiding (HasHttpManager (..), Proxy)
import Network.HTTP.Types.Header as X hiding (Header)
import RIO.Text                  as X (justifyLeft, justifyRight)
import RIO.Time                  as X hiding (getCurrentTime)
import Servant                   as X
import Servant.Client            as X (BaseUrl (..), Scheme (..), showBaseUrl)
import System.Metrics            as X (Store)

-- | Re-exported from Scribes to avoid including package for types only
type ScribeMsg    = Text
type ScribeSource = Text
type ScribeLog    = ScribeSource -> X.LogLevel -> ScribeMsg -> IO ()
