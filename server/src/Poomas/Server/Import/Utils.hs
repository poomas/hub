module Poomas.Server.Import.Utils
  ( tryError
  , throwIfEither

  , fst3, snd3, thd3
  , updLocal
  , mkStatusError

  , prompt

  , listToActiveHubs
  , listToHubs
  , mergeActiveHub
  , mergeActiveHubMaps
  , mergeHub
  , mergeHubMaps
  , insertHub
  , setHubState
  , getCachedHubs
  , getMyself

  , ekgMetric
  , ekgTime
  , time'n'Counter

  , showBUrl
  , showBUrls

  , showVersionHub
  , showPort
  , showMonitor
  , showOffer
  , showHub
  , showHubService
  , showActiveHubs
  , onlyBurls
  , out

  )
where

import           Poomas.Server.Import.CommonImports

import qualified RIO.Text                           as T
import           System.Clock                       (Clock (..), diffTimeSpec, getTime,
                                                     toNanoSecs)


import           Poomas.API                         (ActiveHub (..), Cached (..),
                                                     Hub (..), HubDataState (..),
                                                     HubUri (..), KnownHubs, Monitor (..),
                                                     Offer (..), Version (..), ahHub,
                                                     fromCached, hubDataState, hubNeed,
                                                     hubOffer, hubUri, offerCustom,
                                                     offerGateway, offerInfo,
                                                     offerMonitor, offerService,
                                                     offerSrvService,
                                                     showOfferedServiceFull, showService)
import           Poomas.Server.App                  (ActiveHubs, App (..),
                                                     HasLocalState (..), Hubs,
                                                     lsJsonCached, lsMyself)


------------------------------------------------------------------------------------------
-- Utils
------------------------------------------------------------------------------------------
fst3 :: (a,b,c) -> a
fst3 (x,_,_) = x
{-# INLINE fst3 #-}

snd3 :: (a,b,c) -> b
snd3 (_,x,_) = x
{-# INLINE snd3 #-}

thd3 :: (a,b,c) -> c
thd3 (_,_,x) = x
{-# INLINE thd3 #-}


-- | Show msg on console and wait for any `String`, which is ingored.
prompt :: Text -> IO ()
prompt msg = do
  say msg
  _ <- getLine
  return ()

-- | Convert `MonadError` exception to `Either` type
tryError :: (MonadUnliftIO m, Exception e) => m b -> m (Either e b)
tryError m = (Right <$> m) `catch` (return . Left)

-- | Throw `Exception` if value is `Left`, otherwise return the value in `Right`.
throwIfEither :: (Exception e, MonadThrow m) => Either e a -> m a
throwIfEither = either throwM pure


-- | Update Lens part by supplied function
updLocal :: MonadReader s m => Lens' s a -> (a -> a) -> m v -> m v
updLocal l f = local (\r -> r & l %~ f)


-- | Incorporate text within exception
mkStatusError :: ServerError -> ByteString -> ServerError
mkStatusError e t =
  e { errHeaders = [ (hContentType, "text/plain; charset=utf-8") ]
    , errBody    = toS t
    }

------------------------------------------------------------------------------------------
-- Hub and its maps related functions
------------------------------------------------------------------------------------------

-- | Convert [Hub] to Hubs map
listToHubs :: [Hub] -> Hubs
listToHubs = mapFromList . map ((^. hubUri) &&& id)

-- | Convert [ActiveHub] to ActiveHubs map
listToActiveHubs :: [ActiveHub] -> ActiveHubs
listToActiveHubs = mapFromList . map (getHubUri &&& id)

-- | Merge two ActiveHub maps. On duplicate, Payloads are merged
mergeActiveHubMaps :: ActiveHubs -> ActiveHubs -> ActiveHubs
mergeActiveHubMaps = unionWith mergeActiveHub

-- | Merge two ActiveHub maps. On duplicate, Payloads are merged
mergeHubMaps :: Hubs -> Hubs -> Hubs
mergeHubMaps = unionWith mergeHub

-- | Merge two ActiveHub items. Only Payloads are merged, other data taken from first AH
mergeActiveHub :: ActiveHub -> ActiveHub -> ActiveHub
mergeActiveHub ap1 ap2 = ap1 & ahHub .~ mh (ap1 ^. ahHub) (ap2 ^. ahHub)
  where mh p1 p2 = p1 & hubOffer .~ (p1 ^. hubOffer <> p2 ^. hubOffer)
                      & hubNeed  .~ (p1 ^. hubNeed  <> p2 ^. hubNeed)

-- | Merge two Hub items. Only Payloads are merged
mergeHub :: Hub -> Hub -> Hub
mergeHub (Hub u n1 o1 s1) (Hub _ n2 o2 s2) = Hub u (n1 <> n2) (o1 <> o2) (s1 <> s2)

-- | Insert new `Hub` into `HubsMap` with `Payload` merge
insertHub :: Hub -> Hubs -> Hubs
insertHub p = insertWith mergeHub (p ^. hubUri) p

-- | Set 'Hub' state
setHubState :: HubDataState -> Hub -> Hub
setHubState s = (& hubDataState .~ s)

-- | Fetch Cached KnownHubs from application state
getCachedHubs :: HasLocalState m => m (Cached KnownHubs)
getCachedHubs = view lsJsonCached <$> getLocalState

-- | Fetch configuration representing this 'Hub' - myself
getMyself :: HasLocalState m => m Hub
getMyself = view lsMyself <$> getLocalState

------------------------------------------------------------------------------------------
-- Metrics
------------------------------------------------------------------------------------------

-- | Run EKG function on 'Master's store, if given
ekgMetric :: (MonadIO m, MonadReader App m) => ReaderT Metrics m () -> m ()
ekgMetric meter =
  maybe (pure ()) (runReaderT meter) =<< liftIO . readIORef =<< asks appMetric

-- | Measure time spent in given action under specified name
ekgTime :: (MonadReader App m, MonadUnliftIO m) => Text -> m a -> m a
ekgTime name a = maybe a measureTime =<< liftIO . readIORef =<< asks appMetric
  where
    measureTime m     = bracket (liftIO $ getTime Monotonic) (storeTime m) (const a)
    diffMS end start  = fromInteger @Double (toNanoSecs (diffTimeSpec end start)) / 1_000_000
    storeTime m start = do
      end <- liftIO $ getTime Monotonic
      runReaderT (distribution ("Poomas." <> name) $ diffMS end start) m

-- | Measure time spent in given action under specified name and
--   increase the same name counter by 1
time'n'Counter :: (MonadReader App m, MonadUnliftIO m) => Text -> m a -> m a
time'n'Counter name a = do
  ekgMetric (increment $ "Poomas." <> name <> "-count")
  ekgTime (name <> "-time") a


------------------------------------------------------------------------------------------
-- Display helpers
------------------------------------------------------------------------------------------

-- | Display BaseUrl. Holder must have instance of HubURI
showBUrl :: HubUri a => a -> Text
showBUrl (getHubUri -> BaseUrl{..}) = toS baseUrlHost <> ":" <> tshow baseUrlPort

-- | Display a container of BaseUrls. Each item must have instance of HubURI
showBUrls :: (HubUri a, MonoFoldable (t Text), Functor t, Element (t Text) ~ Text)
          => t a -> Text
showBUrls xs = intercalate "; " $ map showBUrl xs


------------------------------------------------------------------------------------------
-- For debugging
------------------------------------------------------------------------------------------

out :: MonadIO m => Int -> Text -> m ()
out nls m = do
  t <- toS . formatTime defaultTimeLocale "%M:%S" <$> liftIO getCurrentTime
  say $ replicate nls '\n' <> t <> ": " <> m

showVersionHub :: (Version, [BaseUrl]) -> Text
showVersionHub (v,bs) = unVersion v <> ": " <> T.intercalate ", " (map (toS . showBaseUrl) bs)

showPort :: HubUri a => a -> Text
showPort (getHubUri -> BaseUrl{..}) = tshow baseUrlPort

showOffer :: Text -> Offer -> Text
showOffer t o = do
  t <> "Service: " <> disp showOfferedServiceFull (o ^. offerService)
    <> (if null (o ^. offerCustom) then "" else t <> "Custom:  " <> tshow (o ^. offerCustom))
    <> t <> "Info:    " <> disp tshow (o ^. offerInfo)
    <> (if null (o ^. offerMonitor) then "" else t <> "Monitor: " <> disp showMonitor (o ^. offerMonitor))
    <> (if null (o ^. offerGateway) then "" else t <> "Gateway: " <> disp tshow (o ^. offerGateway))
  where
    disp :: Ord a => (a -> Text) -> Set a -> Text
    disp f xs = (t <> "* ") <> intercalate (t <> "* ") (map f $ toList xs)

showMonitor :: Monitor -> Text
showMonitor Monitor{..} = monitorName <> "@" <> tshow monitorEvent

showHub :: Hub -> Text
showHub Hub{..} = showBUrl _hubUri
               <> "\n - Offer: " <> showOffer "\n    - " _hubOffer

showHubService :: Hub -> Text
showHubService Hub{..} =
     showBUrl _hubUri <> ": "
  <> T.intercalate ", \n" (map showOfferedServiceFull . toList $  _hubOffer ^. offerService)


onlyBurls :: Cached KnownHubs -> Text
onlyBurls ps = either (\e -> "Error: " <> toS e) (showBUrls . snd) $ fromCached ps


showActiveHubs :: Map BaseUrl ActiveHub -> Text
showActiveHubs ahs = do
  if null ahs
    then "No hubs!"
    else prefix <> intercalate prefix (map (showSrv . view ahHub) ahs)
  where
    prefix = "\n - "
    showSrv Hub{..} = do
      let offs = toList $ _hubOffer ^. offerService
      showBUrl _hubUri <> " >> " <>
        if null offs
          then ": No services offered!"
          else T.intercalate ";  " (map (showService . view offerSrvService) offs)
