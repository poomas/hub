module TestHubs where

import RIO             hiding (lookup)

import Data.Containers (mapFromList)
import RIO.Set         (fromList)
import Servant.Client  (BaseUrl (..), Scheme (..))


import Poomas.API      (Hub (..), Need (..), Offer (..), OfferedService (..),
                        LoadBalancerType (..), Service (..))
import Poomas.Server   (Hubs)




b :: Int -> BaseUrl
b x = BaseUrl Http (show x) 1 ""

srv1a, srv1b, srv2a, srv3a :: Service
srv1a = Service "s1" "a" LbtRoundRobin
srv1b = Service "s1" "b" LbtRoundRobin
srv2a = Service "s2" "a" LbtRoundRobin
srv3a = Service "s3" "a" LbtRoundRobin

nSrv1a, nSrv1b, nSrv2a, nSrv3a :: Service
nSrv1a =  srv1a
nSrv1b =  srv1b
nSrv2a =  srv2a
nSrv3a =  srv3a

oSrv1a, oSrv1b, oSrv2a, oSrv3a :: Int -> OfferedService
oSrv1a = OfferedService srv1a . b
oSrv1b = OfferedService srv1b . b
oSrv2a = OfferedService srv2a . b
oSrv3a = OfferedService srv3a . b

info1, info2 :: (Text,Text)
info1 = ("Question", "Answer")
info2 = ("Some", "Other")

mkHubElem :: Int -> Need -> Offer -> (BaseUrl, Hub)
mkHubElem (b -> bu) ns os = (bu, Hub bu ns os mempty)

mkOffr :: [OfferedService] -> [(Text, Text)] -> Offer
mkOffr s i = Offer (fromList s) (fromList i) mempty mempty mempty

mkNeed :: [Service] -> Need
mkNeed s = Need (fromList s) mempty mempty mempty

hubState0, hubState1, hubState2 :: Hubs
hubState0 = mapFromList
  [ mkHubElem 2 mempty (mkOffr [oSrv1a 22, oSrv1b 22, oSrv2a 22] [info1])
  , mkHubElem 4 mempty (mkOffr [oSrv1a 44, oSrv1b 44, oSrv2a 44] mempty)
  ]
hubState1 = mapFromList
  [ mkHubElem 1 mempty            (mkOffr [oSrv1a 11, oSrv2a 11, oSrv3a 11] [info1, info2])
  , mkHubElem 7 (mkNeed [nSrv2a]) (mkOffr [oSrv1b 77, oSrv3a 77] mempty)
  , mkHubElem 3 mempty            (mkOffr [oSrv3a 33] mempty)
  , mkHubElem 5 mempty            (mkOffr [oSrv1a 77] mempty)
  , mkHubElem 8 (mkNeed [nSrv3a]) (mkOffr [oSrv2a 88, oSrv3a 88] mempty)
  ]
hubState2 = mapFromList
  [ mkHubElem 6 mempty       (mkOffr [oSrv2a 66] mempty)
  , mkHubElem 7 (mkNeed [nSrv3a]) (mkOffr [oSrv1a 77, oSrv1b 77] mempty)
  , mkHubElem 8 (mkNeed [nSrv1a]) (mkOffr [oSrv1b 88, oSrv3a 88] mempty)
  ]

activeHubs :: Map BaseUrl Hubs
activeHubs = mapFromList [ (b 1,hubState0)
                         , (b 2,hubState1)
                         , (b 3,hubState2)
                         , (b 8,hubState0)
                         ]
