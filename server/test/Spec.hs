{-# OPTIONS_GHC -fno-warn-orphans #-}
import RIO                             hiding (lookup)

import Data.Containers                 (lookup, mapToList)

import Test.Hspec                      (SpecWith, describe, hspec, it, shouldBe)


import Orphans                         ()
import Poomas.API                      (Hub (..), hubUri)
import Poomas.Server                   (RefreshHubs (..), refreshHubs)
import Poomas.Server.Worker.Extractors (byNeededService, byOfferedInfo, byOfferedService)
import TestHubs                        (activeHubs, b, mkNeed, mkOffr, nSrv1a, nSrv2a,
                                        nSrv3a, oSrv1a, oSrv1b, oSrv2a, oSrv3a, srv1a,
                                        srv1b, srv2a, srv3a)



-- type RunClientF = forall a. ClientM a -> IO (Either ClientError a)

instance RefreshHubs Identity where
  refreshHubFetch p = pure
                    . maybe (Left p) (Right . (p,))
                    $ lookup (p ^. hubUri) activeHubs

main :: IO ()
main = do
  hspec $ do

    refreshSpec

    -- just to keep `shouldBe' not used :-(
    -- describe "Fake" $ it "Doesn't matter" $ True `shouldBe` False
    describe "Always" $ it "passes" $ True `shouldBe` True



myself :: Hub
myself = Hub (b 0) mempty mempty mempty

refreshSpec :: SpecWith ()
refreshSpec = do
  let (active, miss) = runIdentity $ refreshHubs myself 4 [b 2, b 4]
      onlyBUs        = map fst  . mapToList

  describe "Refresh" $ do
    it "Active" $ onlyBUs active `shouldBe` map b [1,2,3,8]
    it "Miss"   $ onlyBUs miss   `shouldBe` map b [4,5,6,7]
    it "Active - merged services" $ lookup (b 8) active `shouldBe` Just
      (Hub (b 8) (mkNeed [nSrv1a, nSrv3a])
                 (mkOffr [oSrv1b 88, oSrv2a 88, oSrv3a 88] [])
                  mempty
      )
    it "Miss   - merged services" $ lookup (b 7) miss `shouldBe` Just
      (Hub (b 7) (mkNeed [nSrv2a, nSrv3a])
                 (mkOffr [oSrv1a 77, oSrv1b 77, oSrv3a 77] [])
                  mempty
      )
    it "By offered service and merge service" $ mapToList (byOfferedService active)
      `shouldBe`
               [ (srv1a, ([b 11, b 22],       Nothing))
               , (srv1b, ([b 22, b 88],       Nothing))
               , (srv2a, ([b 11, b 22, b 88], Nothing))
               , (srv3a, ([b 11, b 33, b 88], Nothing))
               ]

    it "By offered info and merge info" $ mapToList (byOfferedInfo active) `shouldBe`
      [ ("Question",[("Answer",[b 1, b 2])])
      , ("Some",    [("Other", [b 1])])
      ]
    it "By needed service" $ mapToList (byNeededService active) `shouldBe`
      [ (srv1a, ([b 8], Nothing))
      , (srv3a, ([b 8], Nothing))
      ]
