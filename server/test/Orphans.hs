{-# OPTIONS_GHC -fno-warn-orphans #-}
module Orphans where

import           Prelude

import qualified Data.Text                      as T

import           Test.QuickCheck
import           Test.QuickCheck.Instances.Time ()

-- | Generate non-empty Text values
instance Arbitrary T.Text where
    arbitrary = T.pack <$> arbitraryNoEmpty
    shrink xs = T.pack <$> shrink (T.unpack xs)

-- | Generate random valid port
arbitraryPort :: Gen Word
arbitraryPort = choose (80,65535)

arbitraryNoEmpty :: Arbitrary a => Gen [a]
arbitraryNoEmpty = getNonEmpty <$> arbitrary
