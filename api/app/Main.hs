module Main where

import           ClassyPrelude              hiding (mapWithKey)
import           RIO                        (Proxy (..))

import           Data.Aeson.Encode.Pretty   (encodePretty)
import           Data.Char                  (isAlphaNum)
import           Data.HashMap.Strict.InsOrd (mapWithKey)
import           Data.String.Conv           (toS)
import           Data.Swagger               (Swagger (..), URL (..), description, info,
                                             license, title, url, version)
import qualified Data.Swagger               as Swg
import           Lens.Micro.Platform        as X hiding (at, (.=))
import qualified Options.Applicative        as O (execParser, fullDesc, header, help,
                                                  helper, info, long, progDesc, short,
                                                  showDefault, strOption, value, (<**>))
import qualified RIO.ByteString.Lazy        as BL
import qualified RIO.Text                   as T
import           Servant.API.Generic        (AsApi, ToServant)
import           Servant.JS                 as JS (CommonGeneratorOptions (..),
                                                   JavaScriptGenerator, camelCase,
                                                   defCommonGeneratorOptions, jqueryWith,
                                                   vanillaJSWith, writeJSForAPI)
import           Servant.Swagger            (HasSwagger (..))
import           System.Directory           (createDirectory, doesDirectoryExist)


import           Poomas.API                 (PoomasRoutes, poomasRoutesApi)
import           PurescriptGen              (exportPoomasToPS
                                            -- , exportPureScriptApi
                                            )


-- | Any cmd line arguments overrides config
newtype Args = Args
  { argOutputDir :: FilePath -- ^ directory where to output swagger and JS files
  } deriving (Show)

-- | Run application server
main :: IO ()
main = do
  Args{..} <- O.execParser pInfo
  unlessM (doesDirectoryExist argOutputDir) $ do
    say $ "Creating directory `" <> toS argOutputDir <> "`"
    createDirectory argOutputDir

  let pursOutputDir = argOutputDir <> "/purescript"

  say $ "Generated files will be stored in directory `" <> toS argOutputDir <> "`"
  generateSwaggerJSON $ argOutputDir <> "/swagger.json"
  -- generateVanillaJS   $ argOutputDir <> "/vanilla-api.js"
  -- generateJQueryJS    $ argOutputDir <> "/jq-api.js"

  exportPoomasToPS pursOutputDir
  -- exportPureScriptApi poomasRoutesApi pursOutputDir


  where
    pInfo = O.info (args O.<**> O.helper)
                 (O.fullDesc <> O.progDesc "API definition for poomas app"
                             <> O.header   "API definition")
    args = Args <$> O.strOption (O.long "outputDir" <> O.short 'o'
                              <> O.value "docs"     <> O.showDefault
                              <> O.help "Directory where to output swagger and JS files")


-- | Create `Swagger` docs for `AllAPIs` API
mkSwagger :: Swagger
mkSwagger = setOperationIds mkOperationId $ toSwagger
  poomasRoutesApi
  & info.title       .~ "Poomas - poor-mans-swarming"
  & info.version     .~ "0.1.0"
  & info.description ?~ "API for poomas microservice hub"
  & info.license     ?~ ("BSD3" & url
                            ?~ URL "https://opensource.org/licenses/BSD-3-Clause"
                        )
  where
    -- Makes concatenated CamelCase string, i.e. "getDelaySeconds"
    mkOperationId verb = T.concat
                       . (:) verb
                       . map T.toTitle
                       . filter (\t -> not (T.null t || "{" `T.isPrefixOf` t))
                       . T.split (\c -> not (isAlphaNum c || c `elem` allowedChars))
    allowedChars = ['-', '{',  '}']



-- | Output generated @swagger.json@ file.
generateSwaggerJSON :: FilePath -> IO ()
generateSwaggerJSON fNm = BL.writeFile fNm $ encodePretty mkSwagger

-- | Generates JavaScript to query the Funcs API.
generateVanillaJS :: FilePath -> IO ()
generateVanillaJS = generateJS vanillaJSWith

-- | Generates JQuery JavaScript to query the Funcs API.
generateJQueryJS :: FilePath -> IO ()
generateJQueryJS = generateJS jqueryWith

-- | Generates JavaScript with specified generator
generateJS :: (CommonGeneratorOptions -> JavaScriptGenerator) -> FilePath -> IO ()
generateJS f = writeJSForAPI (Proxy :: Proxy (ToServant PoomasRoutes AsApi)) (f jsGenOpts)


jsGenOpts :: CommonGeneratorOptions
jsGenOpts = defCommonGeneratorOptions
   { functionNameBuilder = camelCase -- concatCase | snakeCase
   , JS.requestBody      = "body"  -- do not change until fix is released
   , successCallback     = "onSuccess"
   , errorCallback       = "onError"
   , moduleName          = ""
   , urlPrefix           = "/api"
   }

-- | Set 'operationId' to each path with supplied 'convert' function
setOperationIds :: (Text -> Text -> Text) -> Swagger -> Swagger
setOperationIds convert = over Swg.paths (mapWithKey
  (\(T.pack -> path) ->
     let f verb nm  = over verb (setId nm <$>)
         setId verb = over Swg.operationId (<|> Just (convert verb path))
      in f Swg.get     "get"
       . f Swg.put     "put"
       . f Swg.post    "post"
       . f Swg.delete  "delete"
       . f Swg.options "options"
       . f Swg.head_   "head"
       . f Swg.patch   "patch"))
