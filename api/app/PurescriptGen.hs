module PurescriptGen
  ( exportPoomasToPS
  , exportPureScriptApi
  )
where

import ClassyPrelude                       hiding (mapWithKey)
import RIO                                 (Proxy (..))
import RIO.Set                             as Set

import Language.PureScript.Bridge          (PSType, SumType, buildBridge, haskType,
                                            mkSumType, order, psTypeParameters)
import Language.PureScript.Bridge.Builder  (BridgeData, BridgePart, (^==))
import Language.PureScript.Bridge.Printer  (ImportLine (..), importsFromList)
import Language.PureScript.Bridge.TypeInfo (Language (..), TypeInfo (..), typeName)
import Lens.Micro.Platform                 hiding (at, (.=))
import Servant.Foreign                     (Foreign, GenerateList, HasForeign)
import Servant.PureScript                  (HasBridge (..), Settings (..),
                                            defaultSettings, writeAPIModuleWithSettings)
import Servant.PureScript.Internal         (PureScript)


import Poomas.API                          (EventType, Hub, HubDataState,
                                            LoadBalancerType, Monitor, Need, Offer,
                                            OfferedService, Service, Version)
import Common.PurescriptGen                (baseUrlBridge, exportPureScriptTypes,
                                            poomasBridge, setModuleName)


baseTypesDir :: Text
baseTypesDir = "Host."

typeModuleDir :: Text
typeModuleDir = baseTypesDir <> "Type.Poomas."


exportPoomasToPS :: FilePath -> IO ()
exportPoomasToPS = exportPureScriptTypes hubBridge poomasHubTypes

exportPureScriptApi :: ( HasForeign (PureScript HubBridge) PSType api
                       , GenerateList PSType (Foreign PSType api)
                       )
                    => Proxy api -> FilePath -> IO ()
exportPureScriptApi api dir =
  writeAPIModuleWithSettings psApiSettings
                             dir
                             (Proxy :: Proxy HubBridge)
                             api


data HubBridge
instance HasBridge HubBridge where
  languageBridge _ = buildBridge hubBridge

newtype KnownHubs = KnownHubs (Hub,[Hub]) deriving (Generic)


poomasHubTypes :: [SumType 'Haskell]
poomasHubTypes = [ mkSumType (Proxy :: Proxy KnownHubs)
                 , eqOrdType (Proxy :: Proxy Hub)
                 , eqOrdType (Proxy :: Proxy HubDataState)

                 , eqOrdType (Proxy :: Proxy Service)
                 , eqOrdType (Proxy :: Proxy Version)
                 , eqOrdType (Proxy :: Proxy OfferedService)
                 , eqOrdType (Proxy :: Proxy LoadBalancerType)

                 , eqOrdType (Proxy :: Proxy Offer)

                 , eqOrdType (Proxy :: Proxy Need)

                 , eqOrdType (Proxy :: Proxy Monitor)
                 , eqOrdType (Proxy :: Proxy EventType)
                 ]

  where
    eqOrdType p = order p $ mkSumType p



hubBridge :: BridgePart
hubBridge = poomasBridge
  <|> (typeName ^== "SomeCustomEvent" >> psHubType)
  <|> baseUrlBridge (baseTypesDir <> "Request")


  <|> setModuleName typeModuleDir "KnownHubs"        "Hub"
  <|> setModuleName typeModuleDir "Hub"              "Hub"
  <|> setModuleName typeModuleDir "HubDataState"     "Hub"

  <|> setModuleName typeModuleDir "Service"          "Service"
  <|> setModuleName typeModuleDir "Version"          "Service"
  <|> setModuleName typeModuleDir "OfferedService"   "Service"
  <|> setModuleName typeModuleDir "LoadBalancerType" "Service"

  <|> setModuleName typeModuleDir "Offer"            "Offer"

  <|> setModuleName typeModuleDir "Need"             "Need"

  <|> setModuleName typeModuleDir "Monitor"          "Monitor"
  <|> setModuleName typeModuleDir "EventType"        "Monitor"

  where
    psHubType :: MonadReader BridgeData m => m PSType
    psHubType = do
      inType <- view haskType
      params <- psTypeParameters
      pure TypeInfo { _typePackage    = ""
                    , _typeModule     = typeModuleDir <> "PoomasHub"
                    , _typeName       = inType ^. typeName
                    , _typeParameters = params
                    }

psApiSettings :: Settings
psApiSettings = defaultSettings
  { _apiModuleName          = "Hub.WebAPI"
   , _generateSubscriberAPI = True
   , _standardImports       = importsFromList
    [ ImportLine "PreludePlus" mempty
    , ImportLine "Import.PreludeArgonaut" (Set.fromList [ "(.:)"
                                                        , "class DecodeJson"
                                                        , "decodeJson"
                                                        , "jsonParser"
                                                        ])
    ]
  }
