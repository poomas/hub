module Tests.JsonSpec (spec) where

import RIO

import Data.Aeson            (FromJSON, ToJSON, encode, parseJSON, toJSON)
import Data.Aeson.Types      (parseEither)
import Data.String.Conv      (toS)
import Data.Typeable         (typeOf)
import Servant.Swagger       (validateEveryToJSON)
import Test.Hspec            (Spec, describe)
import Test.Hspec.QuickCheck (modifyMaxSize, modifyMaxSuccess, prop)
import Test.QuickCheck       hiding (Result, Success)

import Poomas.API
import Test.Arbitrary        ()


spec :: Spec
spec = do
  describe "JSON to/from conversion"
    . modifyMaxSize (const 30) . modifyMaxSuccess (const 20) $ do
    propJSON (Proxy :: Proxy Version)
    propJSON (Proxy :: Proxy OfferedService)
    propJSON (Proxy :: Proxy Offer)
    propJSON (Proxy :: Proxy Need)
    propJSON (Proxy :: Proxy Hub)
    propJSON (Proxy :: Proxy LoadBalancerType)
    propJSON (Proxy :: Proxy Service)
    propJSON (Proxy :: Proxy PoomasConfig)
    propJSON (Proxy :: Proxy PoomasRefresh)

    -- Monitor part
    propJSON (Proxy :: Proxy Monitor)
    propJSON (Proxy :: Proxy EventType)
    propJSON (Proxy :: Proxy MonitorLoad)
    propJSON (Proxy :: Proxy PoomasEventLoad)
    propJSON (Proxy :: Proxy PoomasLogLoad)
    propJSON (Proxy :: Proxy PoomasTrackLoad)

    -- Message part
    propJSON (Proxy :: Proxy Message)
    propJSON (Proxy :: Proxy MessageType)
    propJSON (Proxy :: Proxy MessageLoad)
    propJSON (Proxy :: Proxy LogFilterLoad)


  describe "JSON to/from conversion - Cached" .
    modifyMaxSize (const 30) . modifyMaxSuccess (const 20) $ do
      propCached (Proxy :: Proxy (Cached Hub))
      propCached (Proxy :: Proxy (Cached KnownHubs))

  describe "All routes: ToJSON matches ToSchema"
    . modifyMaxSize (const 30)
    . modifyMaxSuccess (const 20)
    $ validateEveryToJSON poomasRoutesApi



propJSON :: forall a . (Arbitrary a, ToJSON a, FromJSON a, Show a, Eq a, Typeable a)
         => Proxy a -> Spec
propJSON _ = prop testName $ \(a :: a) ->
  let json = "with " <> toS (encode a)
   in counterexample json (parseEither parseJSON (toJSON a) === Right a)
  where
    testName = show ty <> " FromJSON/ToJSON checks"
    ty       = typeOf (undefined :: a)


propCached :: forall a . (Arbitrary a, Show a, Eq a, Typeable a, CachedValue a)
           => Proxy (Cached a) -> Spec
propCached _ = prop testName $ \(a :: a) -> do
  let item = "with " <> show a
   in counterexample item (fromCached (toCached a) === Right a)
  where
    testName = show ty <> " FromJSON/ToJSON Cached checks"
    ty       = typeOf (undefined :: a)
