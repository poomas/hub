{-# LANGUAGE TemplateHaskell #-}
module Poomas.API.Type.Need
  ( Need (..)

  , needService
  , needInfo
  , needMonitor
  , needGateway

  )
where

import Poomas.API.Imports

import Data.Swagger              as Swg (ToSchema (..), description, example,
                                         genericDeclareNamedSchema, schema)
import RIO.Set                   as S


import Poomas.API.Import.Orphans ()
import Poomas.API.Type.Monitor   (Monitor (..), EventType (..))
import Poomas.API.Type.Service   (LoadBalancerType (..), Service (..))


-- | Need is the data each 'Hub' can use to define what it needs from the swarm.
data Need = Need
  { _needService :: !(Set Service) -- ^ need service w/ suggested LoadBalancerType
  , _needInfo    :: !(Set Text)    -- ^ key/value list of information
  , _needMonitor :: !(Set Monitor) -- ^ JSON, producer/consumer define meaning
  , _needGateway :: !(Set Need)    -- ^ Service for connecting two swarm networks
  } deriving (Show,Eq,Generic,Typeable, Ord)
instance Semigroup Need where
  (Need s1 i1 m1 g1) <> (Need s2 i2 m2 g2) = Need (s1<>s2) (i1<>i2) (m1<>m2) (g1<>g2)
instance Monoid    Need where mempty    = Need mempty mempty mempty mempty
instance ToJSON    Need where toJSON    = genericToJSON    $ jsonOpts 5
instance FromJSON  Need where
  parseJSON = withObject "Need" $ \o -> do
    _needService <- o .:? "service" .!= mempty
    _needInfo    <- o .:? "info"    .!= mempty
    _needMonitor <- o .:? "monitor" .!= mempty
    _needGateway <- o .:? "gateway" .!= mempty
    pure Need{..}
instance ToSchema  Need where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 5) p
    & mapped.schema.description ?~ "Hub needs"
    & mapped.schema.example     ?~ toJSON
        (Need (S.singleton (Service "service1" "v1.2.3" LbtRoundRobin))
              (S.singleton "Question")
              (S.singleton (Monitor "MyName" [EtSwarm]))
              mempty
        )

-- | Keep at end to avoid errors about `undefined`
makeLenses ''Need
