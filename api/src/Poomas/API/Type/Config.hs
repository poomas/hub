{-# LANGUAGE TemplateHaskell #-}
module Poomas.API.Type.Config
  ( PoomasConfig (..)
  , NeedData (..)
  , NeedDataFun
  , PortSetter (..)
  , getPort

  , pCfgBeacons
  , pCfgLocal
  , pCfgRefresh

  , PoomasRefresh (..)
  , srRetryDelaySec
  , srMaxLevels
  , srHeartbeatSec
  )
where

import           Poomas.API.Imports      hiding (Server)

import qualified RIO.Set                as S

import           Poomas.API.Import.Utils (toLoCase)
import           Poomas.API.Type.Hub     (Hub (..), hubNeed, hubOffer, hubUri)
import           Poomas.API.Type.Offer   (offerService)
import           Poomas.API.Type.Service (offerSrvUri)


-- | Main Poomas configuration
data PoomasConfig = PoomasConfig
  { _pCfgBeacons :: ![BaseUrl]     -- ^ list of initial hubs to look for swarm state
  , _pCfgRefresh :: !PoomasRefresh -- ^ swarm state refresh config
  , _pCfgLocal   :: !Hub           -- ^ definition of loacl `Hub`
  } deriving (Show, Eq, Generic)
instance ToJSON   PoomasConfig where
  toJSON PoomasConfig{..} = do
    let BaseUrl{..} = _pCfgLocal ^. hubUri
        path        = bool ("/" <> baseUrlPath) "" (null baseUrlPath)
    object
      [ "beacons" .= _pCfgBeacons
      , "refresh" .= _pCfgRefresh
      , "hubHost" .= (toLoCase (show baseUrlScheme) <> "://" <> baseUrlHost <> path)
      , "hubPort" .= baseUrlPort
      , "offer"   .= (_pCfgLocal ^. hubOffer)
      , "need"    .= (_pCfgLocal ^. hubNeed)
      ]

-- | Configuration for swarm state refreshing
data PoomasRefresh = PoomasRefresh
  { _srRetryDelaySec :: !(Maybe Word) -- ^ secs to wait before 2nd retry, if first failed
  , _srMaxLevels     :: !Word8        -- ^ how many levels to go on refreshing swarm state
  , _srHeartbeatSec  :: !Float        -- ^ perform refresh every heartbeat seconds
  } deriving (Show, Eq, Generic)
instance ToJSON   PoomasRefresh where toJSON    = genericToJSON    $ jsonOpts 3
instance FromJSON PoomasRefresh where parseJSON = genericParseJSON $ jsonOpts 3


makeLenses ''PoomasConfig
makeLenses ''PoomasRefresh


-- | When to set the new port
data PortSetter = PortAlways Word    -- ^ always set the new port
                | PortIfMissing Word -- ^ set port only if current missing
                | PortNever          -- ^ Never set the new port

-- | Type of function used to set the missing data in the `Config` after parse
--   Params:
--   - new host name/IP
--   - new host name
--   - new swarm port
type NeedDataFun = (Maybe String -> Maybe Word -> PortSetter -> PoomasConfig)
newtype NeedData = NeedData {unNeedData :: NeedDataFun}
instance FromJSON NeedData where
  parseJSON = withObject "NeedData" $ \o -> do
    tmpHost <- o .:? "hubHost"
    port    <- o .:  "hubPort"
    pCfg    <- PoomasConfig <$> o .: "beacons"
                            <*> o .: "refresh"
                            <*> do Hub  (extractHost tmpHost)
                                    <$> (o .:? "need"  .!= mempty)
                                    <*> (o .:? "offer" .!= mempty)
                                    <*> pure mempty
    pure . NeedData $ \host hostPort swarmPort ->
      updateOfferedService host (fromIntegral <$> hostPort)
        . setHost host $ setPort (getPort port swarmPort) pCfg

    where
      setPort p        s = s & pCfgLocal . hubUri %~ (\b -> b{baseUrlPort=fromIntegral p})
      setHost (Just h) s = s & pCfgLocal . hubUri %~ (\b -> b{baseUrlHost=h})
      setHost Nothing  s = s

      updateOfferedService host port s = do
        let f = case (host, port) of
                  (Just h,  Nothing) -> (\b -> b{ baseUrlHost = h})
                  (Just h,  Just p)  -> (\b -> b{ baseUrlHost = h
                                                , baseUrlPort = p})
                  (Nothing, Just p)  -> (\b -> b{ baseUrlPort = p})
                  (Nothing, Nothing) -> id
        s & pCfgLocal . hubOffer . offerService %~ S.map (& offerSrvUri %~ f)

      extractHost h =
        fromMaybe (BaseUrl Http "NeedData_need_url" 0 "") $ parseBaseUrl =<< h

-- | Extract port from PortSetter
getPort :: Word -> PortSetter -> Word
getPort currVal = \case
  PortNever       -> currVal
  PortAlways p    -> p
  PortIfMissing p -> bool currVal p $ currVal == 0
