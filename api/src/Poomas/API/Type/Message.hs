module Poomas.API.Type.Message
  ( module X

  , Message (..)
  , MessageData (..)
  , MessageType (..)
  , MessageLoad (..)

  , getMessageDataType
  )
where

import Poomas.API.Imports

import Control.Error                     (note)
import Data.HashMap.Strict.InsOrd        as InsOrd hiding (map)
import Data.Swagger                      as Swg (NamedSchema (..), SwaggerType (..),
                                                 ToParamSchema (..), ToSchema (..),
                                                 declareSchemaRef, description, enum_,
                                                 genericDeclareNamedSchema, properties,
                                                 required, schema, type_)

import Poomas.API.Import.Orphans         ()
import Poomas.API.Import.Utils           (convertType)
import Poomas.API.Type.Message.LogFilter as X


-- | Types of messages that can be sent as direct messages
data MessageData = MessageLog      LogFilterLoad -- ^ log filter message
                 | MessageShutdown ()            -- ^ request to shutdown
                 | MessageCustom   Text          -- ^ custom message, no defined meaning
               deriving (Show, Eq, Ord, Generic)
instance ToJSON   MessageData where toJSON    = genericToJSON    $ jsonOpts 7
instance FromJSON MessageData where parseJSON = genericParseJSON $ jsonOpts 7
instance ToSchema MessageData where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 7) p
    & mapped.schema.description ?~ "Data sent to hub and its master as a direct message"



-- | Type of events that can be used for monitoring swarm state.
--   - log - log from one server send to another, usually Monitor, instance
--   - shutdown - asking instance to shutdown the master and the hub
data MessageType =
    MtLog       -- ^ change log level, filter log, etc.
  | MtShutdown  -- ^ try to shutdown the instance
  | MtCustom
  deriving (Show, Eq, Ord, Generic, Enum, Bounded)
instance ToJSON        MessageType where toJSON    = genericToJSON    $ jsonOpts 2
instance FromJSON      MessageType where parseJSON = genericParseJSON $ jsonOpts 2
instance ToHttpApiData MessageType where
  toUrlPiece MtLog      = "log"
  toUrlPiece MtShutdown = "shutdown"
  toUrlPiece MtCustom   = "custom"
instance FromHttpApiData MessageType where
  parseQueryParam = note "Invalid MessageType  value" . convertType toUrlPiece
instance ToSchema MessageType where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 2) p
    & mapped.schema.description ?~ "Message types that can be sent as direct messages"
instance ToParamSchema MessageType where
 toParamSchema _ = mempty
   & type_ .~ Just SwaggerString
   & enum_ ?~ map (fromString . toS . toUrlPiece) [minBound @MessageType .. maxBound]



-- | Data sent to 'Monitor' service
data MessageLoad = MessageLoad
  { msgLFrom      :: !BaseUrl     -- ^ sender of the message
  , msgLRcptOrder :: !Word8       -- ^ receipt order, 1 - first, 2 - second
  , msgLData      :: !MessageData -- ^ list of URLs
  } deriving (Show, Eq, Ord, Generic)
instance ToJSON   MessageLoad where toJSON    = genericToJSON    $ jsonOpts 4
instance FromJSON MessageLoad where parseJSON = genericParseJSON $ jsonOpts 4
instance ToSchema MessageLoad where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 4) p
    & mapped.schema.description ?~ "Data sent with each direct message"




-- | Offer describing standardised service for collecting events from the swarm.
--   Each `Hub` reports swarm-state change events, with the list of `Hub` added/removed.
data Message = Message
  { messageName  :: !Text          -- ^ name of the Monitor
  , messageEvent :: ![MessageType] -- ^ types of events this Monitor accepts
  } deriving (Show,Ord,Generic,Typeable, Eq)
instance ToJSON   Message where toJSON (Message n e) = object ["name"  .= n, "event" .= e]
instance FromJSON Message where
  parseJSON = withObject "Message" $ \o -> Message <$> o .: "name" <*> o .: "event"

instance ToSchema Message where
  declareNamedSchema _ = do
    nameSchema      <- declareSchemaRef (Proxy :: Proxy Text)
    uriSchema       <- declareSchemaRef (Proxy :: Proxy [String])
    messageSchema   <- declareSchemaRef (Proxy :: Proxy [MessageType])
    pure $ NamedSchema (Just "Monitor") $ mempty
      & description ?~ "A monitor offered or needed by hub"
      & type_       .~ Just SwaggerObject
      & required    .~ [ "name" ]
      & properties  .~ InsOrd.fromList [ ("name",    nameSchema)
                                       , ("uri",     uriSchema)
                                       , ("message", messageSchema)
                                       ]

-- | Convert `MessageData` to `MessageType`
getMessageDataType :: MessageData -> MessageType
getMessageDataType = \case
  (MessageLog _)      -> MtLog
  (MessageShutdown _) -> MtShutdown
  (MessageCustom _)   -> MtCustom
