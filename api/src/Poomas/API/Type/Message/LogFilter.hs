module Poomas.API.Type.Message.LogFilter
  ( LogFilterLoad (..)
  )
where

import Poomas.API.Imports

import Data.Swagger          (ToSchema (..), description, genericDeclareNamedSchema,
                              schema)
import Katip                 (Severity)
import Scribe.Orphans.Scribe ()


-- | Set of data that can be used to filter log messages
data LogFilterLoad = LogFilterLoad
  { lflReset      :: Bool                    -- ^ Reset filter, otherwise data is added
  , lflSeverity   :: Maybe [Severity]        -- ^ list of Severities to allow
  , lflPrefixes   :: Maybe [Text]            -- ^ '-' prefix means exclude
  , lflNamespaces :: Maybe [Text]            -- ^ '-' prefix means exclude
  , lflPeriod     :: Maybe (UTCTime,UTCTime) -- ^ allow only during this period
  , lflHost       :: Maybe String            -- ^ allow only this host
  , lflModule     :: Maybe String            -- ^ only msgs from this source module-Debug
  } deriving (Show, Eq, Ord, Generic)
instance ToJSON   LogFilterLoad where toJSON    = genericToJSON    $ jsonOpts 3
instance FromJSON LogFilterLoad where parseJSON = genericParseJSON $ jsonOpts 3
instance ToSchema LogFilterLoad where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 3) p
    & mapped.schema.description ?~ "Log filtering data"
