module Poomas.API.Type.Monitor
  ( module X

  , Monitor (..)
  , EventData (..)
  , EventType (..)
  , MonitorLoad (..)

  , getEventDataType
  )
where

import Poomas.API.Imports

import Control.Error                   (note)
import Data.HashMap.Strict.InsOrd      as InsOrd hiding (lookup, map)
import Data.Swagger                    as Swg (NamedSchema (..), SwaggerType (..),
                                               ToParamSchema (..), ToSchema (..),
                                               declareSchemaRef, description, enum_,
                                               genericDeclareNamedSchema, properties,
                                               required, schema, type_)

import Poomas.API.Import.Orphans       ()
import Poomas.API.Import.Utils         (convertType)
import Poomas.API.Type.Event.Exception as X
import Poomas.API.Type.Event.Log       as X
import Poomas.API.Type.Event.Poomas    as X
import Poomas.API.Type.Event.Track     as X


-- | Types of messages that each `Monitor` understands
data EventData = EventPoomas    PoomasEventLoad
               | EventTrack     PoomasTrackLoad
               | EventLog       PoomasLogLoad
               | EventException PoomasExceptionLoad
               | EventOther     Text
               deriving (Show, Eq, Ord, Generic)
instance ToJSON   EventData where toJSON    = genericToJSON    $ jsonOpts 5
instance FromJSON EventData where parseJSON = genericParseJSON $ jsonOpts 5
instance ToSchema EventData where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 5) p
    & mapped.schema.description ?~ "Data accompanies event"



-- | Type of events that can be used for monitoring swarm state.
--   - change - some hubs were added to or removed from the 'Hub' state
--   - pong - 'Hub' can occasionally send pong to the 'Monitor', pushing, not polling
data EventType =
    EtSwarm     -- ^ any event pertaining to swarm events
  | EtTrack     -- ^ tracking requests through out the swarm
  | EtLog       -- ^ logging messages. Logging should not be done on per service level
  | EtException -- ^ exceptions that happen in swarm, should be alerted
  | EtOther
  deriving (Show, Eq, Ord, Generic, Enum, Bounded)
instance ToJSON        EventType where toJSON    = genericToJSON    $ jsonOpts 2
instance FromJSON      EventType where parseJSON = genericParseJSON $ jsonOpts 2
instance ToHttpApiData EventType where
  toUrlPiece EtSwarm     = "swarm"
  toUrlPiece EtTrack     = "track"
  toUrlPiece EtLog       = "log"
  toUrlPiece EtException = "exception"
  toUrlPiece EtOther     = "other"
instance FromHttpApiData EventType where
  parseQueryParam = note "Invalid EventType value" . convertType toUrlPiece
instance ToSchema        EventType where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 2) p
    & mapped.schema.description ?~ "Event types Monitor accepts"
instance ToParamSchema   EventType where
 toParamSchema _ = mempty
   & type_ .~ Just SwaggerString
   & enum_ ?~ map (fromString . toS . toUrlPiece) [minBound @EventType .. maxBound]



-- | Set of data sent to 'Monitor' service
data MonitorLoad = MonitorLoad
  { mlFrom      :: !BaseUrl   -- ^ sender of the message
  , mlRcptOrder :: !Word8     -- ^ receipt order, 1 - first, 2 - second
  , mlData      :: !EventData -- ^ list of URLs
  } deriving (Show, Eq, Ord, Generic)
instance ToJSON   MonitorLoad where toJSON    = genericToJSON    $ jsonOpts 2
instance FromJSON MonitorLoad where parseJSON = genericParseJSON $ jsonOpts 2
instance ToSchema MonitorLoad where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 2) p
    & mapped.schema.description ?~ "Data sent with each event"




-- | Offer describing standardised service for collecting events from the swarm.
--   Each `Hub` reports swarm-state change events, with the list of `Hub` added/removed.
data Monitor = Monitor
  { monitorName  :: !Text        -- ^ name of the Monitor
  , monitorEvent :: ![EventType] -- ^ types of events this Monitor accepts
  } deriving (Show,Ord,Generic,Typeable, Eq)
instance ToJSON   Monitor where toJSON (Monitor n e) = object ["name"  .= n, "event" .= e]
instance FromJSON Monitor where
  parseJSON = withObject "Monitor" $ \o -> Monitor <$> o .: "name" <*> o .: "event"

instance ToSchema Monitor where
  declareNamedSchema _ = do
    nameSchema      <- declareSchemaRef (Proxy :: Proxy Text)
    uriSchema       <- declareSchemaRef (Proxy :: Proxy [String])
    eventSchema     <- declareSchemaRef (Proxy :: Proxy [EventType])
    pure $ NamedSchema (Just "Monitor") $ mempty
      & description ?~ "A monitor offered or needed by hub"
      & type_       .~ Just SwaggerObject
      & required    .~ [ "name" ]
      & properties  .~ InsOrd.fromList [ ("name",  nameSchema)
                                       , ("uri",   uriSchema)
                                       , ("event", eventSchema)
                                       ]

getEventDataType :: EventData -> EventType
getEventDataType = \case
  (EventPoomas _)    -> EtSwarm
  (EventTrack _)     -> EtTrack
  (EventLog _)       -> EtLog
  (EventException _) -> EtException
  _                  -> EtOther
