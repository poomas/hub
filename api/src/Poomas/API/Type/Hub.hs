{-# LANGUAGE TemplateHaskell #-}
module Poomas.API.Type.Hub
  ( Hub (..)
  , HubDataState (..)

  , hubUri
  , hubOffer
  , hubNeed
  , hubDataState
  )
where

import Poomas.API.Imports

import Data.Swagger              as Swg (SwaggerType (..), ToParamSchema (..),
                                         ToSchema (..), description, enum_,
                                         fromAesonOptions, genericDeclareNamedSchema,
                                         genericDeclareNamedSchemaUnrestricted, schema,
                                         type_)


import Poomas.API.Import.Orphans ()
import Poomas.API.Type.Need      (Need (..))
import Poomas.API.Type.Offer     (Offer (..))


-- | Possible states of the 'Hub' data
data HubDataState = HdsOld           -- ^ Hub should be updated on next refresh
                  | HdsCurrent       -- ^ Hub could not be updated on next refresh
                  | HdsDeleted Word8 -- ^ Hub is reported as missing, count confirmations
              deriving (Ord, Eq, Generic, Typeable, Show)
instance Semigroup HubDataState where
  HdsCurrent    <> _             = HdsCurrent             -- always prefer Current
  _             <> HdsCurrent    = HdsCurrent
  HdsOld        <> _             = HdsOld                 -- Old is better than Deleted
  _             <> HdsOld        = HdsOld
  HdsDeleted n1 <> HdsDeleted n2 = HdsDeleted $ max n1 n2 -- take the worst
instance Monoid        HubDataState where mempty    = HdsOld
instance ToJSON        HubDataState where toJSON    = genericToJSON    $ jsonOpts 3
instance FromJSON      HubDataState where parseJSON = genericParseJSON $ jsonOpts 3
instance ToHttpApiData HubDataState where
  toUrlPiece HdsOld         = "old"
  toUrlPiece HdsCurrent     = "current"
  toUrlPiece (HdsDeleted _) = "deleted"
instance FromHttpApiData      HubDataState where
  parseQueryParam = \case "old"     -> Right   HdsOld
                          "current" -> Right   HdsCurrent
                          "deleted" -> Right $ HdsDeleted 0
                          _         -> Left "Invalid HubDataState value"
instance ToSchema HubDataState where
  declareNamedSchema p = genericDeclareNamedSchemaUnrestricted (schemaOpts 3) p
    & mapped.schema.description ?~ "Hub data state types"
instance ToParamSchema HubDataState where
 toParamSchema _ = mempty
   & type_ .~ Just SwaggerString
   & enum_ ?~ ["old", "current", "deleted"]


-- | Central element of the swarm.
--   Holds the contact point and offers and needs of its 'Master'
data Hub = Hub
  { _hubUri       :: !BaseUrl      -- ^ URI of the Hub
  , _hubNeed      :: !Need         -- ^ set of needed services and information
  , _hubOffer     :: !Offer        -- ^ set of offered services and information
  , _hubDataState :: !HubDataState -- ^ state of Hub information
  } deriving (Generic, Typeable, Show)
-- instance Show Hub where show (Hub h _ _ _) = showBaseUrl h
instance Eq   Hub where
  (Hub u1 o1 n1 _) == (Hub u2 o2 n2 _) = u1 == u2 && o1 == o2 && n1 == n2
instance Ord Hub where
  (Hub u1 o1 n1 _) `compare` (Hub u2 o2 n2 _) =
    u1 `compare` u2 <> o1 `compare` o2 <> n1 `compare` n2 <> EQ
instance ToJSON   Hub where toJSON    = genericToJSON    $ jsonOpts 4
instance FromJSON Hub where parseJSON = genericParseJSON $ jsonOpts 4
instance ToSchema Hub where
  declareNamedSchema p = genericDeclareNamedSchema (fromAesonOptions $ jsonOpts 4) p
    & mapped.schema.description ?~ "Hub definition"

-- | Keep at end to avoid errors about `undefined`
makeLenses ''Hub
