{-# LANGUAGE TemplateHaskell #-}
module Poomas.API.Type.Offer
  ( Offer (..)

  , offerService
  , offerCustom
  , offerInfo
  , offerMonitor
  , offerGateway

  )
where

import Poomas.API.Imports


import Data.Swagger              as Swg (ToSchema (..), description, example,
                                         genericDeclareNamedSchema, schema)
import RIO.Set                   as S


import Poomas.API.Import.Orphans ()
import Poomas.API.Type.Monitor   (EventType (..), Monitor (..))
import Poomas.API.Type.Service   (LoadBalancerType (..), OfferedService (..),
                                  Service (..))


-- | Offer is the data each 'Hub' can use to define what it offers to swarm.
data Offer = Offer
  { _offerService :: !(Set OfferedService) -- ^ offer service w/ suggested LoadBalancerType
  , _offerInfo    :: !(Set (Text,Text))    -- ^ key/value list of information
  , _offerMonitor :: !(Set Monitor)        -- ^ JSON, producer/consumer define meaning
  , _offerGateway :: !(Set Offer)          -- ^ service for connecting two swarm networks
  , _offerCustom  :: !(Set Value)          -- ^ custom data, undefined meaning
  } deriving (Show,Eq,Generic,Typeable, Ord)
instance Semigroup Offer where
  (Offer s1 c1 i1 m1 g1) <> (Offer s2 c2 i2 m2 g2) =
    Offer (s1 <> s2) (c1 <> c2) (i1 <> i2) (m1 <> m2) (g1 <> g2)
instance Monoid    Offer where mempty    = Offer mempty mempty mempty mempty mempty
instance ToJSON    Offer where toJSON    = genericToJSON    $ jsonOpts 6
instance FromJSON  Offer where
  parseJSON = withObject "Offer" $ \o -> do
    _offerService <- o .:? "service" .!= mempty
    _offerInfo    <- o .:? "info"    .!= mempty
    _offerMonitor <- o .:? "monitor" .!= mempty
    _offerGateway <- o .:? "gateway" .!= mempty
    _offerCustom  <- o .:? "custom"  .!= mempty
    pure Offer{..}

instance ToSchema  Offer where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 6) p
    & mapped.schema.description ?~ "Hub offers"
    & mapped.schema.example     ?~ toJSON
        (Offer (S.singleton (OfferedService (Service "service1" "v1.2.3" LbtRoundRobin)
                                            (BaseUrl Http "localhost" 3003 "")))
               (S.singleton ("Question", "The answer"))
               (S.singleton (Monitor "MyName" [EtSwarm]))
               mempty
               (S.singleton (object ["alive" .= True]))
        )

-- | Keep at end to avoid errors about `undefined`
makeLenses ''Offer
