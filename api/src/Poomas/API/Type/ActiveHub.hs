{-# LANGUAGE TemplateHaskell #-}
module Poomas.API.Type.ActiveHub
  ( ActiveHub (..)
  , Cached (..)
  , CachedValue (..)
  , HubReqs (..)

  , KnownHubs
  , MonitorNotify
  , MessageNotify

  , ahHub
  , ahLastUsed
  , ahPrepedReq

  )
where

import Poomas.API.Imports

import Data.Swagger           as Swg (ToSchema (..))


import Poomas.API.Type.Hub     (Hub (..))
import Poomas.API.Type.Monitor (MonitorLoad)
import Poomas.API.Type.Message (MessageLoad)


-- | Response type of several endpoints. 'Hub' responds with a tuple in which first
--   element is the 'Hub' itself, and second element is a list of known hubs
type KnownHubs = (Hub,[Hub])

-- | Type of function used to send notifications to the 'Monitor'
type MonitorNotify = MonitorLoad -> IO (Either ClientError NoContent)
type MessageNotify = MessageLoad -> IO (Either ClientError NoContent)

-- | A `Hub` that is currently active/alive in the swarm. Holds information
--     - about the `Hub`
--     - when it was last used//refreshed//accessed
--     - its cached representation as ByteString, JSON or custom form
--     - its fully prepared client requests
data ActiveHub = ActiveHub
  { _ahHub       :: !Hub     -- ^ hub this belongs to
  , _ahLastUsed  :: !UTCTime -- ^ last time used
  , _ahPrepedReq :: !HubReqs -- ^ list of fully prepared client functions
  } deriving (Generic, Typeable)
instance Ord ActiveHub where compare = comparing _ahHub <> comparing _ahLastUsed
instance Eq  ActiveHub where
  a == b = all (\f -> f a b) [ (==) `on` _ahHub
                             -- , (==) `on` _ahLastUsed
                             ]

-- | List of fully prepared client requests for specific `Hub`
data HubReqs = HubReqs
  { prAlive           ::               IO  Bool                     -- ^ is hub alive
  , prPoomasStateGet  ::               IO (Either String KnownHubs) -- ^ get hub's state
  , prPoomasStatePost :: Cached Hub -> IO (Either String KnownHubs) -- ^ send&get hub's state
  , prMonitorPost     :: MonitorNotify                              -- ^ send monitor event
  , prMessagePost     :: MessageNotify                              -- ^ send direct message
  } deriving (Generic, Typeable)


-- | Possible cached representation of some data (mostly current swarm state)
data Cached a =
    AsData a                -- ^ no cache, Servant will convert it to JSON
  | AsValue Value           -- ^ half conversion, state cached as Value, conversion to BS
  | AsByteString ByteString -- ^ no conversion, state cached as ByteString
  deriving (Show, Eq, Generic, Typeable)
instance FromJSON a => FromJSON (Cached a) where parseJSON a = AsData <$> parseJSON a
instance ToJSON   a => ToJSON   (Cached a) where
  toJSON (AsData  a) = toJSON a
  toJSON (AsValue v) = v
  toJSON (AsByteString s) = fromMaybe (object["errorConversion" .= (toS s :: String)])
                                      (decode $ toS s :: Maybe Value)
instance (ToSchema a) => ToSchema (Cached a) where
  declareNamedSchema _ = declareNamedSchema (Proxy :: Proxy a)


-- | Class for converting values to Cached form
class CachedValue a where
  fromCached :: Cached a -> Either String a -- ^ restore cached value to its original form
  toCached   :: a        -> Cached a        -- ^ convert original value to cached form

-- | Instance for converting 'KnownHubs' to Cached form
instance CachedValue KnownHubs where
  toCached (p,ps) =
    -- AsValue $ toJSON (p, ps)
    AsByteString . toS $ encode (p, ps)
  fromCached = \case AsValue o -> case fromJSON o of
                       Success (p, ps) -> Right (p,ps)
                       Error e         -> Left e
                     AsByteString js -> case eitherDecode $ toS js of
                       Right (p,ps) -> Right (p,ps)
                       Left e       -> Left e
                     AsData a       -> Right a

-- | Instance for converting 'Hub' to Cached form
instance CachedValue Hub where
  toCached p =
    -- AsValue $ toJSON p
    AsByteString . toS $ encode p
  fromCached = \case AsValue js -> case fromJSON js of
                       Success p -> Right p
                       Error e   -> Left e
                     AsByteString js -> case eitherDecode $ toS js of
                       Right p -> Right p
                       Left e  -> Left e
                     AsData a -> Right a

-- | Keep at end to avoid errors about `undefined`
makeLenses ''ActiveHub
