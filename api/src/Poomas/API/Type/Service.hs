{-# LANGUAGE TemplateHaskell #-}
module Poomas.API.Type.Service
  ( Service (..)
  , OfferedService (..)
  , Version (..)
  , LoadBalancerType (..)

  , serviceName
  , serviceVersion
  , serviceLBType

  , offerSrvService
  , offerSrvUri

  , showLoadBalancer
  , showService
  , showOfferedServiceFull

  )
where

import Poomas.API.Imports


import Data.Swagger            as Swg (NamedSchema (..), SwaggerType (..), ToSchema (..),
                                       declareSchemaRef, description, example,
                                       genericDeclareNamedSchema, properties, required,
                                       schema, type_)

import Poomas.API.Import.Utils (toLoCase)



-- | Type representing 'Version' (of the 'Service'). Just a pure Text, so any kind of
--   version designation can be used. Undefined for swarm, consumer must understand it.
newtype Version = Version {unVersion :: Text }
  deriving (Show,Read,Eq,Ord,Generic,Typeable,ToHttpApiData,FromHttpApiData
           , FromJSON, ToJSON, IsString)
instance ToSchema Version where
  declareNamedSchema _ = pure . NamedSchema (Just "Version") $ mempty
    & type_       ?~ SwaggerString
    & description ?~ "Service version string"
    & example     ?~ "v1.2.3.4"



-- | Algorithms for load-balancing between Tween services when fetching their 'BaseUrl'.
data LoadBalancerType =
    LbtRoundRobin  -- ^ Round-robin balancer, circling all the items
  | LbtAlwaysFirst -- ^ always select first in the list
  deriving (Show, Eq, Ord, Generic)
instance ToJSON   LoadBalancerType where toJSON = showLoadBalancer
instance FromJSON LoadBalancerType where
  parseJSON (String "round-robin")  = pure LbtRoundRobin
  parseJSON (String "always-first") = pure LbtAlwaysFirst
  parseJSON _                       = mzero
instance ToSchema LoadBalancerType where
  declareNamedSchema _ = pure . NamedSchema (Just "LoadBalancerType") $ mempty
    & description ?~ "Type of tween balancer for these services"
    & example     ?~ "round-robin or always-first"



-- | Service defined in the swarm. It can be offered and needed.
--   LoadBalancerType is the one recommended by the 0Master' offering service.
--   Consumer can decide to use another 'LoadBalancerType'.
data Service = Service
  { _serviceName    :: !Text             -- ^ name of the service
  , _serviceVersion :: !Version          -- ^ version designation
  , _serviceLBType  :: !LoadBalancerType -- ^ algorithm for load-balancing if Twin service
  } deriving (Generic,Typeable,Show)
-- instance Show     Service where show (Service n (Version v) _) = toS $ n <> ":" <> v
instance Eq       Service where (Service n v _) == (Service n2 v2 _) = n == n2 && v == v2
instance Ord      Service where
  (Service n1 v1 _) `compare` (Service n2 v2 _) = n1 `compare` n2 <> v1 `compare` v2 <> EQ
instance ToJSON   Service where toJSON = genericToJSON $ jsonOpts 8
instance FromJSON Service where
  parseJSON = withObject "Service" $ \o -> do
    _serviceName    <- o .:  "name"
    _serviceVersion <- o .:  "version"
    _serviceLBType  <- o .:? "load-balancer-type" .!= LbtRoundRobin
    pure Service{..}
instance ToSchema Service where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 8) p
    & mapped.schema.description ?~ "A service offered or needed by hub"



-- | Service offered to the swarm.
data OfferedService = OfferedService
  { _offerSrvService :: !Service -- ^ offered Service
  , _offerSrvUri     :: !BaseUrl -- ^ URL on which this service/version can be accessed
  } deriving (Generic,Typeable, Show, Eq, Ord)
instance ToJSON OfferedService where
  toJSON (OfferedService srv (BaseUrl s h p u)) = object
    [ "service" .= srv
    , "path"    .= (toLoCase (show s) <> "://" <> h <> ":" <> show p <> u)
    ]
instance FromJSON OfferedService where
  parseJSON = withObject "OfferedService" $ \o -> do
    _offerSrvService <-                                     o .: "service"
    _offerSrvUri     <- maybe mzero pure . parseBaseUrl =<< o .: "path"
    pure OfferedService{..}
instance ToSchema OfferedService where
  declareNamedSchema _ = do
    serviceSchema <- declareSchemaRef (Proxy :: Proxy Service)
    uriSchema     <- declareSchemaRef (Proxy :: Proxy String)
    pure $ NamedSchema (Just "OfferedService") $ mempty
      & description ?~ "An offered service by hub"
      & type_       ?~ SwaggerObject
      & required    .~ ["service", "path"]
      & properties  .~ fromList [ ("service", serviceSchema)
                                , ("path",    uriSchema)
                                ]



------------------------------------------------------------------------------------------
-- Helper functions
------------------------------------------------------------------------------------------
showLoadBalancer :: IsString p => LoadBalancerType -> p
showLoadBalancer LbtRoundRobin  = "round-robin"
showLoadBalancer LbtAlwaysFirst = "always-first"

showService :: Service -> Text
showService Service{..} = _serviceName <> ":" <> unVersion _serviceVersion

showOfferedServiceFull :: OfferedService -> Text
showOfferedServiceFull OfferedService{..} =
  showService _offerSrvService
           <> "@" <> toS (showBaseUrl _offerSrvUri)
           <> "<?>" <> showLoadBalancer (_serviceLBType _offerSrvService)

-- | Keep at end to avoid errors about `undefined`
makeLenses ''Service
makeLenses ''OfferedService
