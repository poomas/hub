module Poomas.API.Type.Event.Log
  ( PoomasLogLoad (..)
  )
where

import Poomas.API.Imports

import Data.Swagger       (ToSchema (..), description, genericDeclareNamedSchema, schema)


-- | Log message format. `pllMessage` should be JSON encoded `Katip`'s `Item` object
data PoomasLogLoad = PoomasLogLoad
  { pllOrigin  :: Text  -- ^ who sent the log message
  , pllMessage :: Text  -- ^ JSON representation of the Katip's Item object
  } deriving (Show, Eq, Ord, Generic)
instance ToJSON   PoomasLogLoad where toJSON    = genericToJSON    $ jsonOpts 3
instance FromJSON PoomasLogLoad where parseJSON = genericParseJSON $ jsonOpts 3
instance ToSchema PoomasLogLoad where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 3) p
    & mapped.schema.description ?~ "Data sent for logging"
