module Poomas.API.Type.Event.Track
  ( PoomasTrackLoad (..)
  )
where

import Poomas.API.Imports

import Data.Swagger       (ToSchema (..), description, genericDeclareNamedSchema, schema)
import Data.UUID          (UUID)




data PoomasTrackLoad = PoomasTrackLoad
  { ptlUuid    :: UUID
  , ptlMessage :: Text
  } deriving (Show, Eq, Ord, Generic)
instance ToJSON   PoomasTrackLoad where toJSON    = genericToJSON    $ jsonOpts 3
instance FromJSON PoomasTrackLoad where parseJSON = genericParseJSON $ jsonOpts 3
instance ToSchema PoomasTrackLoad where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 3) p
    & mapped.schema.description ?~ "Data sent for tracking"
