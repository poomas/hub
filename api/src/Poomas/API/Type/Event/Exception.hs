module Poomas.API.Type.Event.Exception
  ( PoomasExceptionLoad (..)
  )
where

import Poomas.API.Imports

import Data.Swagger       (ToSchema (..), description, genericDeclareNamedSchema, schema)


-- | Exception message format. `pexlMessage` is textual description of the exception
data PoomasExceptionLoad = PoomasExceptionLoad
  { pexlOrigin  :: Text -- ^ who sent the exception
  , pexlMessage :: Text -- ^ exception description
  } deriving (Show, Eq, Ord, Generic)
instance ToJSON   PoomasExceptionLoad where toJSON    = genericToJSON    $ jsonOpts 3
instance FromJSON PoomasExceptionLoad where parseJSON = genericParseJSON $ jsonOpts 3
instance ToSchema PoomasExceptionLoad where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 3) p
    & mapped.schema.description ?~ "Data sent for logging"
