module Poomas.API.Type.Event.Poomas
  ( PoomasEventLoad (..)

  , examplePoomasEventLoad
  )
where

import Poomas.API.Imports

import Data.Swagger       (ToSchema (..), description, genericDeclareNamedSchema, schema)



-- | Set of data sent to 'Monitor' service
data PoomasEventLoad = PoomasEventLoad
  { pelHubs    :: ![Text]      -- ^ list of URLs
  , pelMessage :: !Text        -- ^ free form message
  } deriving (Show, Eq, Ord, Generic)
instance ToJSON   PoomasEventLoad where toJSON    = genericToJSON    $ jsonOpts 3
instance FromJSON PoomasEventLoad where parseJSON = genericParseJSON $ jsonOpts 3
instance ToSchema PoomasEventLoad where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 3) p
    & mapped.schema.description ?~ "Types of Poomas events monitor can track"


examplePoomasEventLoad :: PoomasEventLoad
examplePoomasEventLoad = PoomasEventLoad
  { pelHubs    = []
  , pelMessage = ""
  }
