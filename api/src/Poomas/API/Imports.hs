module Poomas.API.Imports
  ( module X

  , jsonOpts
  , schemaOpts
  , fixJSONField
  )
where

import           RIO                        as X hiding (Handler, view)

import           Data.Aeson                 as X
import           Data.Aeson.Types           as X
import           Data.HashMap.Strict.InsOrd as X (fromList)
import           Data.String.Conv           as X
import           Data.Swagger               as Swg (SchemaOptions (..), fromAesonOptions)
import           Debug                      as X
import           Lens.Micro.Platform        as X hiding (at, (.=))
import           RIO.Time                   as X
import           Servant                    as X
import           Servant.Client             as X

import qualified Data.Char                  as C


-- | Options for automatic FromJSON and ToJSON generation.
--  Just lower-cases the first letter of the constructor.
jsonOpts :: Int -> Options
jsonOpts n = defaultOptions
  { X.sumEncoding            = ObjectWithSingleField
  , X.unwrapUnaryRecords     = True
  , X.constructorTagModifier = fixJSONField n
  , X.fieldLabelModifier     = fixJSONField n
  }

-- | Options for defining Schema
schemaOpts :: Int -> SchemaOptions
schemaOpts n = (fromAesonOptions $ jsonOpts n)
  { Swg.constructorTagModifier = fixJSONField n
  , Swg.unwrapUnaryRecords     = True
  }

fixJSONField :: Int -> String -> String
fixJSONField n t = case drop n t of
                     (x:xs) -> C.toLower x : xs
                     _      -> []
