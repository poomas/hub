module Poomas.API.Import.Utils where

import           Poomas.API.Imports

import qualified RIO.Char          as C


-- | Make URL from BaseUrl and URI (from Link)
mkUrl :: BaseUrl -> URI -> String
mkUrl b = mappend (showBaseUrl b <> "/") . show


-- | Convert String to lower case.
toLoCase :: String -> String
toLoCase = map C.toLower


-- | Convert type with given function from one form to another.
--   Usually from String/Text to Type
convertType :: (Eq s, Bounded a, Enum a) => (a -> s) -> s -> Maybe a
convertType f s = lookup s $ map (f &&& id) [minBound..maxBound]
