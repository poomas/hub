{-# OPTIONS_GHC -fno-warn-orphans #-}
module Poomas.API.Import.Orphans where

import Poomas.API.Imports

import Data.Swagger        (NamedSchema (..), SwaggerItems (..), SwaggerType (..),
                            ToSchema (..), defaultSchemaOptions, description, example,
                            genericDeclareNamedSchemaUnrestricted, items, type_)





------------------------------------------------------------------------------------------
-- Orphan instances for NoContent
------------------------------------------------------------------------------------------

instance ToJSON   NoContent where toJSON NoContent = Array mempty
instance FromJSON NoContent where
  parseJSON (Array v) | null v = pure NoContent
  parseJSON v                  = typeMismatch "NoContent" v
instance ToSchema NoContent where
  declareNamedSchema _ = pure . NamedSchema Nothing $ mempty
    & type_       .~ Just SwaggerArray
    & items       ?~ SwaggerItemsArray []
    & description ?~ "No content"

instance ToSchema BaseUrl where
  declareNamedSchema _ = pure . NamedSchema (Just "URI") $ mempty
    & type_       .~ Just SwaggerString
    & description ?~ "Base URI of hub"
    & example     ?~ "localhost:3004"

------------------------------------------------------------------------------------------
-- Orphan instances for Aeson's Value
------------------------------------------------------------------------------------------
instance ToSchema Value where
  declareNamedSchema = genericDeclareNamedSchemaUnrestricted defaultSchemaOptions
