module Poomas.API
  ( module X

  -- Class
  , HubUri (..)

  -- Routes
  , PoomasRoutes (..)

  -- API
  , AllRoutes
  , PoomasRoutesProxy
  , poomasRoutesApi
  , poomasRoutesProxy

  -- Links
  , poomasLinks
  , aliveLget
  , poomasStateLget
  , poomasStateLpost
  , refreshLget
  , monitorLpost
  , messageLpost

  -- Client funcs
  , poomasClientRoutes
  , aliveC
  , poomasStateGetC
  , poomasStatePostC
  , refreshC
  , monitorC
  , messageC

  -- Utils
  , mkActiveHub
  , mkUri
  )
where

import Poomas.API.Imports


import Poomas.API.Import.Orphans as X ()
import Poomas.API.Import.Utils   as X
import Poomas.API.Type.ActiveHub as X
import Poomas.API.Type.Config    as X
import Poomas.API.Type.Hub       as X
import Poomas.API.Type.Message   as X
import Poomas.API.Type.Monitor   as X
import Poomas.API.Type.Need      as X
import Poomas.API.Type.Offer     as X
import Poomas.API.Type.Service   as X



import Network.HTTP.Client       (Manager)
import Servant.API.Generic       ((:-), AsApi, ToServant, ToServantApi, fromServant,
                                  genericApi)
import Servant.Client.Generic    (AsClientT)


-- | Class for easy access to URI of a 'Hub'
class    HubUri a           where getHubUri :: a -> BaseUrl
instance HubUri Hub         where getHubUri = view hubUri
instance HubUri ActiveHub   where getHubUri = getHubUri . view ahHub
instance HubUri BaseUrl     where getHubUri = id
instance HubUri (BaseUrl,a) where getHubUri = fst


-- | Create an active hub with all its client functions
mkActiveHub :: Manager -> UTCTime -> Hub -> ActiveHub
mkActiveHub mgr lastUsed h = do
  let cEnv              = mkClientEnv mgr $ h ^. hubUri
      prPoomasStateGet  = mkPoomasStateGetC  cEnv
      prPoomasStatePost = mkPoomasStatePostC cEnv
      prAlive           = mkAliveC cEnv
      prMonitorPost     = mkMonitorC cEnv
      prMessagePost     = mkMessageC cEnv
  ActiveHub h lastUsed HubReqs{..}

  where

    mkPoomasStateGetC :: ClientEnv -> IO (Either String KnownHubs)
    mkPoomasStateGetC = checkHubs poomasStateGetC

    mkPoomasStatePostC :: ClientEnv -> Cached Hub -> IO (Either String KnownHubs)
    mkPoomasStatePostC cEnv bcp = checkHubs (poomasStatePostC bcp) cEnv

    mkMonitorC :: ClientEnv -> MonitorNotify
    mkMonitorC cEnv ml = runClientM (monitorC ml) cEnv

    mkMessageC :: ClientEnv -> MessageNotify
    mkMessageC cEnv ml = runClientM (messageC ml) cEnv

    mkAliveC :: ClientEnv -> IO Bool
    mkAliveC cEnv = (\case Right (Right b) -> b
                           _               -> False
                    ) <$> try @_ @SomeException (runClientM aliveC cEnv)

    checkHubs a cEnv = (\case Right (Right c) -> fromCached c
                              Right (Left e)  -> Left $ show e
                              Left e          -> Left $ show e
                       ) <$> try @_ @SomeException (runClientM a cEnv)



------------------------------------------------------------------------------------------
-- Routes/API
------------------------------------------------------------------------------------------

type PoomasRoutesProxy = Proxy (ToServantApi PoomasRoutes)
type AllRoutes        = PoomasRoutes

poomasRoutesProxy :: Proxy (ToServant PoomasRoutes AsApi)
poomasRoutesProxy = Proxy

poomasRoutesApi :: PoomasRoutesProxy
poomasRoutesApi = genericApi (Proxy :: Proxy PoomasRoutes)


type AliveRoute =
     Summary "Is Hub alive?"
  :> Get '[JSON] Bool

type PoomasStateGetRoute =
     Summary "Return current swarm state known to this Hub"
  :> Get '[JSON] (Cached KnownHubs)

type PoomasStatePostRoute =
     Summary "Accept swarm state from caller and return own current swarm state"
  :> ReqBody '[JSON] (Cached Hub)
  :> Post    '[JSON] (Cached KnownHubs)

type RefreshRoute =
     Summary "Force refresh of the the current swarm state"
  :> Get '[JSON] (Cached KnownHubs)

type MonitorRoute =
     Summary "Accept notification events from the Hub in the swarm"
  :> ReqBody '[JSON] MonitorLoad
  :> Post '[JSON] NoContent

type MessageRoute =
     Summary "Accept direct messages and relays them to master"
  :> ReqBody '[JSON] MessageLoad
  :> Post '[JSON] NoContent


data PoomasRoutes r = PoomasRoutes
  { poomasAliveR     :: !(r :- "alive"   :> AliveRoute)
  , poomasStateGetR  :: !(r :- "state"   :> PoomasStateGetRoute)
  , poomasStatePostR :: !(r :- "state"   :> PoomasStatePostRoute)
  , poomasRefreshR   :: !(r :- "refresh" :> RefreshRoute)
  , poomasMonitorR   :: !(r :- "monitor" :> MonitorRoute)
  , poomasMessageR   :: !(r :- "direct"  :> MessageRoute)
  } deriving (Generic)


------------------------------------------------------------------------------------------
-- Links
------------------------------------------------------------------------------------------

poomasLinks :: PoomasRoutes (AsLink Link)
poomasLinks = allFieldLinks


aliveLget :: URI
aliveLget  = mkUri poomasAliveR
poomasStateLget :: URI
poomasStateLget = mkUri poomasStateGetR
poomasStateLpost :: URI
poomasStateLpost = mkUri poomasStatePostR
refreshLget :: URI
refreshLget = mkUri poomasRefreshR
monitorLpost :: URI
monitorLpost = mkUri poomasMonitorR
messageLpost :: URI
messageLpost = mkUri poomasMessageR

mkUri :: (PoomasRoutes (AsLink Link) -> Link) -> URI
mkUri f = linkURI $ f poomasLinks


------------------------------------------------------------------------------------------
--  Client functions
------------------------------------------------------------------------------------------

poomasClientRoutes :: PoomasRoutes (AsClientT ClientM)
poomasClientRoutes = fromServant $ client (Proxy :: Proxy (ToServant PoomasRoutes AsApi))

aliveC :: ClientM Bool
aliveC = poomasAliveR poomasClientRoutes

poomasStateGetC :: ClientM (Cached KnownHubs)
poomasStateGetC = poomasStateGetR poomasClientRoutes

poomasStatePostC :: Cached Hub -> ClientM (Cached KnownHubs)
poomasStatePostC = poomasStatePostR poomasClientRoutes

refreshC :: ClientM (Cached KnownHubs)
refreshC = poomasRefreshR poomasClientRoutes

monitorC :: MonitorLoad -> ClientM NoContent
monitorC = poomasMonitorR poomasClientRoutes

messageC :: MessageLoad -> ClientM NoContent
messageC = poomasMessageR poomasClientRoutes
