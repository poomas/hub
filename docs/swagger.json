{
    "swagger": "2.0",
    "info": {
        "version": "0.1.0",
        "title": "Poomas - poor-mans-swarming",
        "license": {
            "url": "https://opensource.org/licenses/BSD-3-Clause",
            "name": "BSD3"
        },
        "description": "API for poomas microservice hub"
    },
    "definitions": {
        "EventData": {
            "minProperties": 1,
            "maxProperties": 1,
            "type": "object",
            "description": "Data accompanies event",
            "properties": {
                "log": {
                    "$ref": "#/definitions/PoomasLogLoad"
                },
                "other": {
                    "type": "string"
                },
                "track": {
                    "$ref": "#/definitions/PoomasTrackLoad"
                },
                "exception": {
                    "$ref": "#/definitions/PoomasExceptionLoad"
                },
                "poomas": {
                    "$ref": "#/definitions/PoomasEventLoad"
                }
            }
        },
        "OfferedService": {
            "required": [
                "service",
                "path"
            ],
            "type": "object",
            "description": "An offered service by hub",
            "properties": {
                "path": {
                    "type": "string"
                },
                "service": {
                    "$ref": "#/definitions/Service"
                }
            }
        },
        "Hub": {
            "required": [
                "uri",
                "need",
                "offer",
                "dataState"
            ],
            "type": "object",
            "description": "Hub definition",
            "properties": {
                "dataState": {
                    "$ref": "#/definitions/HubDataState"
                },
                "offer": {
                    "$ref": "#/definitions/Offer"
                },
                "uri": {
                    "$ref": "#/definitions/URI"
                },
                "need": {
                    "$ref": "#/definitions/Need"
                }
            }
        },
        "MonitorLoad": {
            "required": [
                "from",
                "rcptOrder",
                "data"
            ],
            "type": "object",
            "description": "Data sent with each event",
            "properties": {
                "data": {
                    "$ref": "#/definitions/EventData"
                },
                "rcptOrder": {
                    "maximum": 255,
                    "minimum": 0,
                    "type": "integer"
                },
                "from": {
                    "$ref": "#/definitions/URI"
                }
            }
        },
        "LogFilterLoad": {
            "required": [
                "reset"
            ],
            "type": "object",
            "description": "Log filtering data",
            "properties": {
                "period": {
                    "minItems": 2,
                    "items": [
                        {
                            "$ref": "#/definitions/UTCTime"
                        },
                        {
                            "$ref": "#/definitions/UTCTime"
                        }
                    ],
                    "maxItems": 2,
                    "type": "array"
                },
                "namespaces": {
                    "items": {
                        "type": "string"
                    },
                    "type": "array"
                },
                "severity": {
                    "items": {
                        "$ref": "#/definitions/Severity"
                    },
                    "type": "array"
                },
                "prefixes": {
                    "items": {
                        "type": "string"
                    },
                    "type": "array"
                },
                "module": {
                    "type": "string"
                },
                "reset": {
                    "type": "boolean"
                },
                "host": {
                    "type": "string"
                }
            }
        },
        "LoadBalancerType": {
            "example": "round-robin or always-first",
            "description": "Type of tween balancer for these services"
        },
        "Service": {
            "required": [
                "name",
                "version",
                "lBType"
            ],
            "type": "object",
            "description": "A service offered or needed by hub",
            "properties": {
                "name": {
                    "type": "string"
                },
                "version": {
                    "$ref": "#/definitions/Version"
                },
                "lBType": {
                    "$ref": "#/definitions/LoadBalancerType"
                }
            }
        },
        "UUID": {
            "example": "00000000-0000-0000-0000-000000000000",
            "format": "uuid",
            "type": "string"
        },
        "MessageLoad": {
            "required": [
                "from",
                "rcptOrder",
                "data"
            ],
            "type": "object",
            "description": "Data sent with each direct message",
            "properties": {
                "data": {
                    "$ref": "#/definitions/MessageData"
                },
                "rcptOrder": {
                    "maximum": 255,
                    "minimum": 0,
                    "type": "integer"
                },
                "from": {
                    "$ref": "#/definitions/URI"
                }
            }
        },
        "Offer": {
            "example": {
                "service": [
                    {
                        "path": "http://localhost:3003",
                        "service": {
                            "name": "service1",
                            "version": "v1.2.3",
                            "lBType": "round-robin"
                        }
                    }
                ],
                "gateway": [],
                "custom": [
                    {
                        "alive": true
                    }
                ],
                "monitor": [
                    {
                        "event": [
                            "swarm"
                        ],
                        "name": "MyName"
                    }
                ],
                "info": [
                    [
                        "Question",
                        "The answer"
                    ]
                ]
            },
            "required": [
                "service",
                "info",
                "monitor",
                "gateway",
                "custom"
            ],
            "type": "object",
            "description": "Hub offers",
            "properties": {
                "service": {
                    "uniqueItems": true,
                    "items": {
                        "$ref": "#/definitions/OfferedService"
                    },
                    "type": "array"
                },
                "gateway": {
                    "uniqueItems": true,
                    "items": {
                        "$ref": "#/definitions/Offer"
                    },
                    "type": "array"
                },
                "custom": {
                    "uniqueItems": true,
                    "items": {
                        "$ref": "#/definitions/Value"
                    },
                    "type": "array"
                },
                "monitor": {
                    "uniqueItems": true,
                    "items": {
                        "$ref": "#/definitions/Monitor"
                    },
                    "type": "array"
                },
                "info": {
                    "uniqueItems": true,
                    "items": {
                        "minItems": 2,
                        "items": [
                            {
                                "type": "string"
                            },
                            {
                                "type": "string"
                            }
                        ],
                        "maxItems": 2,
                        "type": "array"
                    },
                    "type": "array"
                }
            }
        },
        "Severity": {
            "example": "Debug",
            "type": "string",
            "enum": [
                "Debug",
                "Info",
                "Warning",
                "Error",
                "Notice",
                "Critical",
                "Alert",
                "Emergency"
            ],
            "description": "Severity defines which log messages to log"
        },
        "Value": {
            "minProperties": 1,
            "maxProperties": 1,
            "type": "object",
            "properties": {
                "String": {
                    "type": "string"
                },
                "Null": {
                    "example": [],
                    "items": {},
                    "maxItems": 0,
                    "type": "array"
                },
                "Array": {
                    "items": {
                        "$ref": "#/definitions/Value"
                    },
                    "type": "array"
                },
                "Object": {
                    "$ref": "#/definitions/Object"
                },
                "Number": {
                    "type": "number"
                },
                "Bool": {
                    "type": "boolean"
                }
            }
        },
        "PoomasExceptionLoad": {
            "required": [
                "lOrigin",
                "lMessage"
            ],
            "type": "object",
            "description": "Data sent for logging",
            "properties": {
                "lOrigin": {
                    "type": "string"
                },
                "lMessage": {
                    "type": "string"
                }
            }
        },
        "URI": {
            "example": "localhost:3004",
            "type": "string",
            "description": "Base URI of hub"
        },
        "PoomasTrackLoad": {
            "required": [
                "uuid",
                "message"
            ],
            "type": "object",
            "description": "Data sent for tracking",
            "properties": {
                "uuid": {
                    "$ref": "#/definitions/UUID"
                },
                "message": {
                    "type": "string"
                }
            }
        },
        "Monitor": {
            "required": [
                "name"
            ],
            "type": "object",
            "description": "A monitor offered or needed by hub",
            "properties": {
                "event": {
                    "items": {
                        "$ref": "#/definitions/EventType"
                    },
                    "type": "array"
                },
                "uri": {
                    "items": {
                        "type": "string"
                    },
                    "type": "array"
                },
                "name": {
                    "type": "string"
                }
            }
        },
        "EventType": {
            "type": "string",
            "enum": [
                "swarm",
                "track",
                "log",
                "exception",
                "other"
            ],
            "description": "Event types Monitor accepts"
        },
        "UTCTime": {
            "example": "2016-07-22T00:00:00Z",
            "format": "yyyy-mm-ddThh:MM:ssZ",
            "type": "string"
        },
        "Need": {
            "example": {
                "service": [
                    {
                        "name": "service1",
                        "version": "v1.2.3",
                        "lBType": "round-robin"
                    }
                ],
                "gateway": [],
                "monitor": [
                    {
                        "event": [
                            "swarm"
                        ],
                        "name": "MyName"
                    }
                ],
                "info": [
                    "Question"
                ]
            },
            "required": [
                "service",
                "info",
                "monitor",
                "gateway"
            ],
            "type": "object",
            "description": "Hub needs",
            "properties": {
                "service": {
                    "uniqueItems": true,
                    "items": {
                        "$ref": "#/definitions/Service"
                    },
                    "type": "array"
                },
                "gateway": {
                    "uniqueItems": true,
                    "items": {
                        "$ref": "#/definitions/Need"
                    },
                    "type": "array"
                },
                "monitor": {
                    "uniqueItems": true,
                    "items": {
                        "$ref": "#/definitions/Monitor"
                    },
                    "type": "array"
                },
                "info": {
                    "uniqueItems": true,
                    "items": {
                        "type": "string"
                    },
                    "type": "array"
                }
            }
        },
        "Version": {
            "example": "v1.2.3.4",
            "type": "string",
            "description": "Service version string"
        },
        "PoomasLogLoad": {
            "required": [
                "origin",
                "message"
            ],
            "type": "object",
            "description": "Data sent for logging",
            "properties": {
                "origin": {
                    "type": "string"
                },
                "message": {
                    "type": "string"
                }
            }
        },
        "Object": {
            "additionalProperties": true,
            "type": "object",
            "description": "Arbitrary JSON object."
        },
        "PoomasEventLoad": {
            "required": [
                "hubs",
                "message"
            ],
            "type": "object",
            "description": "Types of Poomas events monitor can track",
            "properties": {
                "hubs": {
                    "items": {
                        "type": "string"
                    },
                    "type": "array"
                },
                "message": {
                    "type": "string"
                }
            }
        },
        "HubDataState": {
            "minProperties": 1,
            "maxProperties": 1,
            "type": "object",
            "description": "Hub data state types",
            "properties": {
                "old": {
                    "example": [],
                    "items": {},
                    "maxItems": 0,
                    "type": "array"
                },
                "current": {
                    "example": [],
                    "items": {},
                    "maxItems": 0,
                    "type": "array"
                },
                "deleted": {
                    "maximum": 255,
                    "minimum": 0,
                    "type": "integer"
                }
            }
        },
        "MessageData": {
            "minProperties": 1,
            "maxProperties": 1,
            "type": "object",
            "description": "Data sent to hub and its master as a direct message",
            "properties": {
                "log": {
                    "$ref": "#/definitions/LogFilterLoad"
                },
                "shutdown": {
                    "example": [],
                    "items": {},
                    "maxItems": 0,
                    "type": "array"
                },
                "custom": {
                    "type": "string"
                }
            }
        }
    },
    "paths": {
        "/direct": {
            "post": {
                "summary": "Accept direct messages and relays them to master",
                "consumes": [
                    "application/json;charset=utf-8"
                ],
                "responses": {
                    "400": {
                        "description": "Invalid `body`"
                    },
                    "200": {
                        "description": ""
                    }
                },
                "produces": [
                    "application/json;charset=utf-8"
                ],
                "parameters": [
                    {
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/MessageLoad"
                        },
                        "in": "body",
                        "name": "body"
                    }
                ],
                "operationId": "postDirect"
            }
        },
        "/refresh": {
            "get": {
                "summary": "Force refresh of the the current swarm state",
                "responses": {
                    "200": {
                        "schema": {
                            "minItems": 2,
                            "items": [
                                {
                                    "$ref": "#/definitions/Hub"
                                },
                                {
                                    "items": {
                                        "$ref": "#/definitions/Hub"
                                    },
                                    "type": "array"
                                }
                            ],
                            "maxItems": 2,
                            "type": "array"
                        },
                        "description": ""
                    }
                },
                "produces": [
                    "application/json;charset=utf-8"
                ],
                "operationId": "getRefresh"
            }
        },
        "/alive": {
            "get": {
                "summary": "Is Hub alive?",
                "responses": {
                    "200": {
                        "schema": {
                            "type": "boolean"
                        },
                        "description": ""
                    }
                },
                "produces": [
                    "application/json;charset=utf-8"
                ],
                "operationId": "getAlive"
            }
        },
        "/state": {
            "post": {
                "summary": "Accept swarm state from caller and return own current swarm state",
                "consumes": [
                    "application/json;charset=utf-8"
                ],
                "responses": {
                    "400": {
                        "description": "Invalid `body`"
                    },
                    "200": {
                        "schema": {
                            "minItems": 2,
                            "items": [
                                {
                                    "$ref": "#/definitions/Hub"
                                },
                                {
                                    "items": {
                                        "$ref": "#/definitions/Hub"
                                    },
                                    "type": "array"
                                }
                            ],
                            "maxItems": 2,
                            "type": "array"
                        },
                        "description": ""
                    }
                },
                "produces": [
                    "application/json;charset=utf-8"
                ],
                "parameters": [
                    {
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/Hub"
                        },
                        "in": "body",
                        "name": "body"
                    }
                ],
                "operationId": "postState"
            },
            "get": {
                "summary": "Return current swarm state known to this Hub",
                "responses": {
                    "200": {
                        "schema": {
                            "minItems": 2,
                            "items": [
                                {
                                    "$ref": "#/definitions/Hub"
                                },
                                {
                                    "items": {
                                        "$ref": "#/definitions/Hub"
                                    },
                                    "type": "array"
                                }
                            ],
                            "maxItems": 2,
                            "type": "array"
                        },
                        "description": ""
                    }
                },
                "produces": [
                    "application/json;charset=utf-8"
                ],
                "operationId": "getState"
            }
        },
        "/monitor": {
            "post": {
                "summary": "Accept notification events from the Hub in the swarm",
                "consumes": [
                    "application/json;charset=utf-8"
                ],
                "responses": {
                    "400": {
                        "description": "Invalid `body`"
                    },
                    "200": {
                        "description": ""
                    }
                },
                "produces": [
                    "application/json;charset=utf-8"
                ],
                "parameters": [
                    {
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/MonitorLoad"
                        },
                        "in": "body",
                        "name": "body"
                    }
                ],
                "operationId": "postMonitor"
            }
        }
    }
}