-- File auto generated by purescript-bridge! --
module Host.Type.Poomas.Monitor where

import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Show (genericShow)
import Data.Lens (Iso', Lens', Prism', lens, prism')
import Data.Lens.Iso.Newtype (_Newtype)
import Data.Lens.Record (prop)
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype)
import Data.Symbol (SProxy(SProxy))

import Prelude

newtype Monitor
  = Monitor
      { monitorName :: String
      , monitorEvent :: Array EventType
      }


derive instance eqMonitor :: Eq Monitor
derive instance ordMonitor :: Ord Monitor
derive instance genericMonitor :: Generic Monitor _
derive instance newtypeMonitor :: Newtype Monitor _
--------------------------------------------------------------------------------
_Monitor :: Iso' Monitor { monitorName :: String
                         , monitorEvent :: Array EventType }
_Monitor = _Newtype
--------------------------------------------------------------------------------
data EventType
  = EtSwarm
  | EtTrack
  | EtLog
  | EtException
  | EtOther


derive instance eqEventType :: Eq EventType
derive instance ordEventType :: Ord EventType
derive instance genericEventType :: Generic EventType _
--------------------------------------------------------------------------------
_EtSwarm :: Prism' EventType Unit
_EtSwarm = prism' (\_ -> EtSwarm) f
  where
    f EtSwarm = Just unit
    f _ = Nothing

_EtTrack :: Prism' EventType Unit
_EtTrack = prism' (\_ -> EtTrack) f
  where
    f EtTrack = Just unit
    f _ = Nothing

_EtLog :: Prism' EventType Unit
_EtLog = prism' (\_ -> EtLog) f
  where
    f EtLog = Just unit
    f _ = Nothing

_EtException :: Prism' EventType Unit
_EtException = prism' (\_ -> EtException) f
  where
    f EtException = Just unit
    f _ = Nothing

_EtOther :: Prism' EventType Unit
_EtOther = prism' (\_ -> EtOther) f
  where
    f EtOther = Just unit
    f _ = Nothing
--------------------------------------------------------------------------------
