# `Poomas` hub

Please read the [introduction](Home.Md) first, would be easier grasp the following.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [`Poomas` hub](#poomas-hub)
    - [Some definitions](#some-definitions)
    - [Hub internal operations](#hub-internal-operations)
        - [Hub payload](#hub-payload)
            - [Service](#service)
            - [Monitor](#monitor)
    - [Master](#master)
    - [Using Hub by the Master](#using-hub-by-the-master)
        - [Hooks](#hooks)
            - [Info hook](#info-hook)
            - [Monitor hook](#monitor-hook)
            - [Message hook](#message-hook)
        - [Example code for starting `Hub` process](#example-code-for-starting-hub-process)
            - [Functions for communicating with "owned" `Hub`](#functions-for-communicating-with-owned-hub)
                - [Dynamic configuration](#dynamic-configuration)
                - [Setting hooks](#setting-hooks)
                - [Controlling the behaviour](#controlling-the-behaviour)
                - [Gathering information](#gathering-information)
    - [Swarm configuration YAML example](#swarm-configuration-yaml-example)
    - [poomas-server - a standalone `Hub`](#poomas-server---a-standalone-hub)

<!-- markdown-toc end -->



## Some definitions

`Hub` is a library and it is a standalone server, with its own port.

`Master` is a the app that offers service(s) to the swarm and each `Master` owns its own
`Hub`.

`Master` communicates with `Hub` via `PoomasHubComm` channel, and `Hub` informs the
`Master` about the swarm events via `hooks`.


<img src="images/Master-Hub.png">


Each `Hub` in the swarm is at the same time the swarm state data producer and swarm state
data consumer (from other hub's). Every hub in the swarm is treated equal. There are
no master-client relations. All `Hub`s are peers.


`Hub` and `Master` are symbionts. They live their own lives, separately, and provide what
the other one needs. `Hub` gives its `Master` information about the services URL and, if
given hooks, about the events in the swarm. `Master` gives `Hub` logging functions and EKG
`Store`, if logging and performace statistics are wanted by the `Master`.

That is all the communication between the two. None can affect the process of the other.
With the only exception that `Master` can stop/start `Hub`, as it is its owner.



## Hub internal operations

`Hub` communicates with other `Hub`s by sharing and exchanging information about the swarm
state currently known to the `Hub`.

`Hub` initiates refresh every `heartbeatSec` seconds (a float defined in configuration).
This is a concurrent process and does not interfere with `Hub`s other processes.

It always starts with the list initial beacons given in config. For that reason, at least
one active beacon should always be present in the swarm at known IP address (or name).
This is the only slight deviation from "non central point" logic. Although, it is not one
point, nevertheless. However, when the swarm is up, those initial beacons are no longer
necessarry. `Hub` will query any other `Hub` that it knows about.

Each beacon is in turn asked for its known swarm state. States from all queried beacons is
gathered and combined to form new current state of the swarm. There is some optimization,
so that a hub whose data we have is not queried again. The maximum number of requests in a
refresh tasks from all the hubs is (n * 2 - 1) (where n is the number of known hubs in the
swarm). So for 5 nodes, max cost of the refresh of all hubs is 9 requests.

Any `Hub` can contact another `Hub` not only for refresh, but also to trigger an event or
pass a message, either to the `Hub` itself, or to its `Master`.

Swarm state consists of a list of `Hub`s with their payload (all the needs and offers of
their respective `Master`s).



### Hub payload

`Hub` payload consists of its `Master`'s information, both `Offer`s and `Need`s:

  1. `Service` - name & version of the service, suggested `load balancer`, and the `URI`
     (for `Offer`ed service).

  2. `Monitor` - name of the monitor and the events it accepts (currently only `Swarm`,
     `Track`, `Log`, `Exception` and `Other`).

  3. Named `Info` - list of textual values for `Need`s and tuples (i.e. ('key', 'value'))
     for `Offer`s. `Info` should not be used for real-time messaging. It is part of the
     swarm state and is disspiated via refresh events. This is mor like an announcement.

  4. `Custom` - any JSON objects `Master` wants to make publicly available. Consumer must
      understand its meaning.

  5. `Gateway` - `Master` can act as a bridge between different swarms. (Only designed,
      not implemented yet.)


Any part of the payload can be dynamically added, changed and removed by the `Master` they
belong to.



#### Service

`Hub` keeps a list of `Need`ed services in a separate list for faster access. Same service
provided by several `Master`s are called `replica`. When there are `replica`s, `Hub`
provides a mean of load-balancing. Currently only two are developed:

  1. `RoundRobin` - one after another
  2. `AlwaysFirst` - always pick the first in the list. Can be used when services are
     sorted by priorities, i.e. main and backup.



#### Monitor

`Hub` understands how to exchange `Monitor` events. `Monitor` currently understands for
types of events:

  1. `Swarm` event - internal event for `Hub`. `Master` can never receive suh an event.
  2. `Track` event - some items/messages going through the swarm from one service to
     another sometimes need tracking. (not fully developed yet)
  3. `Log` event - when centralised logging is implemented, log messages are sent via this
     event
  4. `Exception` event - when an unhandled exception occurs in a service, which usually
     results in 500 code sent, a service should send this event to a monitor service and
     and inform it, so such exceptions can be tracked. They can also be sent as a log
     message, but this way a specialized service can be developed that does not need to
     filter each log message.
  5. `Other` event - undefined textual information. Consumer should understand the format
     and the meaning of the content.

It is important to emphesise that `Hub` does not do anything with any of these events, it
just passes them to its `Master` that has to implement all the logic about handling them.


If there is no service that offers `Monitor` service, monitoring is inactive. None of the
messages will be sent.

If `Monitor` name is * , all `Hub`s offering `Monitor` service will be notified. If any name
other than "*" is specified, only `Hub`s offering `Monitor` by that name will be notified.



## Master

`Master` is a user application providing some information and service(s) to others, and
also using some information and services from other members of the swarm. `Master` owns
a `Hub`.

`Master` is responsible to configure its `Hub`, start it and enable its refresh process.
Once configured and run, `Hub` needs nothing else from its `Master`. Most of the
configuration options can be changed dynamically, except for the domain and the port for
the `Hub`. If such change is needed, `Hub` must be restarted with such new configuration.

`Hub` is capable to log its own messages and provide performance statistics. If `Master`
wants to enable those, it must provide the `EKG` metrics store and the logging function.

If `Master` wants to participate in some special swarm events, it should give its `Hub`
hooks for such events. Events comming to these hooks can happen ata any time, so `Master`
should be able to handle them asynchronuosly.

Currently there are three types of hooks:

  1. Info hook - when an `Info` which `Master` needs changes or disappears from the state,
     this hook is activated. This hook is not activated if an `Info`, that is not
     configured as `Need`ed, changes or appears.

  2. Monitor hook - if `Master` announced that it offers `Monitor`, this hook should be
     implemented to handle monitor events (i.e. log, exception, track). `Master` can
     specify which type of messages can it handle, maybe only a subset

  3. Direct message hook - direct messages are sent via this hook. If it is not
     implemented, `Master` will not be able to receive ans direct message (real-time
     messaging).


`Master` informs its `Hub` about the information and services it offers and about the
services and information it needs. That can be accomplished during initial configuration
of the `Hub` or dynamically later via `PoomacHubComm`.



## Using Hub by the Master



### Hooks



#### Info hook

When an `Info`, that is configured in `Need` section appears, disappears or changes,
`Master` is informed about it. The hook must have this signature

``` haskell
  infoHook :: [(TextKey,[(TextValue,[BaseUrl])])] -> IO ()
```

`Master` receives a list of `Info`s via this hook. Each item is defined by the key it
represents and holds a list of values of which each holds a lit of services offering it.

Example:
``` haskell
  [ "Question A", [ "Answer 1", ["hub1", "hub2"]
                  , "Answer 2", ["hub4", "hub8"]
                  ]
  , "Question B", [ "Answer 5", ["hubQ", "hubW"]]
  ]
```

For example, many different `Master`s can provide, for example, their `EKG` host and port,
so a consumer of that info can access each one and gather EKG data in one place.


It is up to `Master` to make sense of this and take an appropriate action to the choosen
`Hub`.




#### Monitor hook

When a `Monitor` message arrives, `Master` is informed about it. The hook must have this
signature:

``` haskell
  data MonitorLoad = MonitorLoad
    { mlFrom      :: BaseUrl
    , mlRcptOrder :: Word8
    , mlData      :: EventData
    }
  monitorHook :: MonitorLoad -> IO ()
```

Load has data about the message originator, the recepient order (*) and the load. For types
of load see Monitor section above, or documentation for detailed explanation.


(*) If recepient order is 2, it means someone else has already received and processed this
message. If there are several `replica`s, maybe only the first one will act on certain
type of message.



#### Message hook

When a direct message arrives, `Master` is informed about it. The hook must have this
signature:

``` haskell
  messageHook :: MessageLoad -> IO ()
```

There are currently defined three types of messages:
  1. Log message - containes the data for filtering the log messages
  2. Shutdown message - `Master` should shutdown its `Hub`, release resources and die
  3. Custom message - textual info, undefined, consumer of the message must understand its
     format and meaning






### Example code


#### Starting `Hub` process with `common` package

To start the `Hub`, it must be given at the very least a domain and a port it will bind to,
and a non-empty list of initial URLs.

``` haskell
main =
  withKillSwitch prepConfig prepApp $ \(AppConfig{..}, AppPrep{..}, phcEnv) -> do
    startHubRefresher phcEnv
    setEKGStore       phcEnv . Just $ (capMetrics . appCommon $ apApp) ^. metricsStore
    setLogger         phcEnv . rioToPoomasLog $ apApp
    setMessageHook    phcEnv $ Just (messageHook apApp)
```



##### Get information

``` haskell
getBaseUrl :: PhcEnv -> Service -> IO (Maybe BaseUrl)
```

The most often used function by the `Master`. It is called to obtain the current URL of the
wanted service. `Hub` knows which active `Master`(s) are providing that service and will
provide it (via load-balancing if service has more replicas).


``` haskell
getOfferedInfo :: PhcEnv -> Text -> IO [(TextValue, [BaseUrl])]
```

Finds the Info in the swarm state and returna all the values with the list of services
offering that value.



#### `Hub` configuration

``` yaml
poomas:
  hubPort: 0                                 # get free port
  beacons:
    - http://localhost:3016                  # this one is surely up and running
  need:
    service:
      - name:    "some-service"              # need service by this name
        version: "v1.2.3"                    # and this version
    monitor:
      - name:  "*"                           # no name, always send to me
        event: [ "swarm"                     # can handle all types of Monitor messages
               , "track"
               , "log"
               , "exception"
               , "other"
               ]
    info:
      - "question"                           # need values for these Info
      - "query"
  offer:
    service:
      - service:
          name:               "my-service"   # offerinf service under this name
          version:            "v2.1"         # and this version
          load-balancer-type: "round-robin"  # just use next and wrap for next replica
        path:     "/v2"                      # service is offered on this path on Master
    info:
      - ["EKG port", "3333"]                 # let others know where my stats are
    custom:
      - city:      "Zagreb"
      - addresses: ["best street", "24"]
      - blanky:
      - is-name: true
  refresh:
    # retryDelaySec: 1                       # *1
    maxLevels:    5                          # *2 on refresh, do not go deeper than 5 levels
    heartbeatSec: 3.5                        # *3 refresh state every 3.5 seconds
```

*1 `delaySec` - if network is suffering from occasional interruptions, you can specify
                delay to retry the query if first one fails.

*2 `maxLevels` - how many levels deep will refresher sub-process follow the children.

*3 `heartbeatSec` - how often will `Hub` refresh its state.
