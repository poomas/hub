(work in progress)
<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Poomas - poor-mans-swarming system](#poomas---poor-mans-swarming-system)
    - [Motivation](#motivation)
        - [Why micro-services?](#why-micro-services)
    - [Short description of `poomas` functionality](#short-description-of-poomas-functionality)
        - [Special services](#special-services)
        - [Security](#security)
        - [Logging](#logging)
    - [When `poomas` becomes inadequate](#when-poomas-becomes-inadequate)
    - [Including solutions in other languages as a micro-service (a `Master`)](#including-solutions-in-other-languages-as-a-micro-service-a-master)
    - [Short summary of current `Poomas` features](#short-summary-of-current-poomas-features)
    - [Which are the main libraries used by `poomas` and common functions?](#which-are-the-main-libraries-used-by-poomas-and-common-functions)

<!-- markdown-toc end -->



# Poomas - poor-mans-swarming system

`Poomas` is a crash-resistant, self-sustainable (soon), decentralised, zero-configuration,
auto-discovery micro-services swarm management system. Well, this is a mouthful, isn't it?
All true, but keep in mind its name, it is meant for people poor with resources, money,
time, people, knowledge, expertise, .... you name it. It's not designed with large
swarms in mind. More of a way to split a big monolith app into smaller parts without
(too) much hassle and/or change in code.

It doesn't do anything special that other such systems, like Kubernetes, Jeager, Nomad,
etc. do. In fact, it does less, much less. So, what was the motivation for creating
something like this?



## Motivation

I needed a micro-services management system that would be very light, without any central
point (of failure), be able to (partially) function even when some of its components fail,
easy to reconfigure and restructure and, most importantly, something that I can understand,
use and change without spending a lot of time learning new stuff for any new functionality
want, but I actually do not need for app development, although have to be aware of.



### Why micro-services?

Many years of app design and development taught me that whatever we plan in the beginning
and predict for the future, always ends up wrong and incomplete. Apps have their own life,
and after several iterations, they start to look more like the picture of Dorian Gray than
our beautiful initial design. More and more features are being added, of which some simply
do not fit nicely into the original design. We squeeze them in, but slowly start to lose
confident of app being indestructible in all its parts. Haskell makes many of such problems
the past memories with its ease of safe refactoring, but as the app grows, well, you get
the picture.


Micro-services approach give us also few other perks for free, like really quick and easy
on-boarding new devs, partial hot code-reload, not polluting the main app with client's
customisation, quick and easy introduction of new features for testing, much easier
maintenance, etc.

Also, one of the very large impact benefit is that you can horizontally scale only some
services to answer the high demand, instead of the whole applications.



## Short description of `poomas` functionality

Main idea is that each service is not just a dumb client in the swarm, but also a hub. An
entity that knows everything there is to know about the current state of the swarm (to be
precise, here "current" means the last refresh time), so it can share its swarm data with
other members of the swarm.

Each `poomas` entity consists of two parts, the `Hub`, which handles all the swarm events,
and the `Master`, the service itself. The `Hub` is completely separate from the `Master`,
has its own IP port and can't interfere with its `Master` in any way. `Hub` has the life
of its own. `Master` can't influence on how the `Hub` works, it can only change the
information about its own service offered to the swarm, set some information tuples and,
of course, can stop/start the `Hub` (if swarm reconfiguration requests so).

`Hub` is not aware of the capabilities of its `Master`, it is generic and can only pass
information between the swarm and the `Master`. The `Master`, to be able to react to swarm
events, should connect to its `Hub` hooks.

Visual representation of the `Master`/`Hub` entities in the swarm.

<img src="images/Poomas-Swarm.png">

For the `Master`, the most often used function will be to ask it `Hub` for the URL of some
other needed service. No matter how the swarm re-configures, that information would always
be current (i.e. as of last state refresh). If two of the same (name and version) services
exist in the swarm, access to them can be load-balanced (currently only two balancers
present, RoundRobin and AlwaysFirst).


`Master`s can share information with other `Master`(s) in the swarm via `direct messages`
or via `Info tuples`. Former is in real-time and does not persist in the swarm, while the
latter persist in the swarm's state, but spreads with the swarm state, via refresh of each
`Hub`.

More detailed description at [Hub](Hub.md)



### Special services

All `Hub`s in the swarm are treated equal, however there are some services that are
considered special and are therefore given global presence and are recognised by the
`Hub`s.

One of them is the `Monitor` service which should understand and act upon several types
of messages:

  - `Poomas` messages about some swarm events
  - `Log` messages for implementing centralised logging
  - `Exception` messages about unhandled exceptions
  - `Track` messages about items flowing through the swarm that should be tracked

One `Monitor` does not have to act on every type of message. One `Monitor` can just collect
log messages, and the other only exceptions. However, all those types of messages should be
handled by the swarm somehow.

The other (not yet implemented) special service will be `Gateway`, for accessing outside
world and exposing the swarm to the world, but in a secure manner.



### Security

Poomas currently uses `HTTP`, but the system is fully prepared to use `HTTPS`. No special
security restrictions are given. It is supposed to run inside a closed network with limited
exposure/access.



### Logging

`Poomas` defines special messages to be used for logging. This way all entities in the swarm
can log to central place(s). `Katip` framework offers easy way to implement different
`Scribe`s (i.e. log output) and that feature is used to implement logging over network via
standard `Katip` log functions. Beside centralised logging, one can define additional
`Scribe`s and log messages will be routed to several outputs, like to console, to `Monitor`
and to file or ElasticSearch. Each service should always log to `poomas`, and additional
`Scribes` used when needed. `Poomas` currently offers only three: console, file and
ElasticSearch.



## When `poomas` becomes inadequate

Since `poomas` is designed for smaller number of micro-services, the application can/will
outgrow its possibilities, or some swarm functionalities will be missing, and a need to
switch to other solution will be needed.

To deal with such a situation, there are three possible paths:

1. If the needed/missing functionality is not complex, just develop it inside `poomas`.

   `poomas`'s code is very simple and it actually does not do anything other than
   maintaining the state of the swarm. Everything else, for example centralized logging and
   exception handling, is left to the `Master`. `Poomas` acts only as an intermediary or
   proxy, relaying structured messages between different `Master` services. So adding a new
   message or hook should mostly be easy.

2. If the needed functionality is complex, make a `Master` that acts as a proxy.

   If you do not want to develop it yourself, but use a ready made solution, make a
   `Master` that acts as a proxy between the `poomas` swarm and that solution.

3. If nothing of the above is applicable

    Abandon poomas and replace it's functionality with the choosen solution functionalities.
    Even though this sounds as very complex, it can prove not to be. `Poomas` is designed
    in such a way that it has indeed a minimal interference with its `Master`. Basic usage,
    beside setting up and running the `Hub`, is only the `getBaseUrl` function (which gives
    the current URL of the wanted service). Hooks are the other thing, but not every
    service will use the hooks (beside the provided standard ones).



## Including solutions in other languages as a micro-service (a `Master`)

`Poomas` is written in Haskell, so cannot be easily directly used by other languages.
However, because of its design, very similar usage can be achieved. The main difference
between Haskell solution vs. solution in another language is that there's no direct
communication bridge between the `Master` and its `Hub`, so all the communication should
go via `Hub`'s REST API (yellow fat arrow on the image above) (not yet exposed).

For that purpose a [beacon](https://gitlab.com/poomas/beacon) can be used as a
template. It is a standalone `Master`/`Hub` entity that does not provide any other
service, but acts only as a node in the swarm. It has a very low memory footprint (<5MB).



## Short summary of current `Poomas` features

- zero-configuration (currently only port, IP and/or network interface)

  No need to specify static IP address for the Hub nor its Master (configurable)

- service auto-discovery

  Each service announces to the swarm what it offers and tells its `Hub` what it needs

- network logging

  Logs are being sent to any number of dedicated loggers, which can then persist logs in
  one or more places, spread logs to different consumers based on any characteristic. To
  enable log messages from the `Hub`, `Master` must give it a proper log function.

- dynamic log filtering

  Centralised logger can send information about log filter to any service, and those
  unwanted log messages will not be sent over the network, therefore saving resources.
  Log filtering can be done based on the origin (the IP or service), based on severity,
  message content, origin, namespace and even source code module (Debug mode only).

- unhandled exception management

  Any exception not properly handled by the service where it occurred (i.e. 500), can be
  sent to any number of registered exception handlers

- direct messaging between hubs and services

  A message can be sent in real-time to any number of entities, specified either by the IP
  or by the service name. Direct messages are not persisted in the swarm.

- information broadcasting

  Part of the local `Hub` state is also a list of text tuples, key-value data. That list
  is part of the state, so it is not sent in real-time, but each `Hub` gets it during the
  state refresh. Unlike direct messages, this info remains the part of the swarm until the
  Only originating `Master` can remove it from the state. It also disappears if service
  dies.

- performance metrics

  Each `Hub` can collect various statistics about itself. To activate it, `Master` must
  provide [EKG store](https://hackage.haskell.org/package/ekg-core/docs/System-Metrics.html).
  EKG package also provides nice HTML dynamic display of the data (not yet shared by
  `poomas`).

- friendly shutdown of any hub

  `Poomas` has a standard shutdown message and a `Master` should know how to respond. In
  such an event, `Master` should first shutdown its `Hub`, release all the resources and
  then shutdown itself.

- templates for new service to minimise the effort

  To ease the service development, a repository with
  [common functions](https://gitlab.com/poomas/common) is provided. It is highly
  opinionated, but can serve as a guide to how to implement it. The main purpose of such
  repository to unify services behaviour and simplify creation of new services.
  [beacon](https://gitlab.com/poomas/beacon) is an example of how little work is needed
  to make a new service and can serve as a template.



## Which are the main libraries used by `poomas` and common functions?

`Poomas` is built using these main packages:

- Aeson
- EKG
- Katip
- RIO
- Servant


## Example services

[- Tracker](https://gitlab.com/poomas/tracker)

  A `Monitor` service accepting all `Monitor` events and provides centralised logging to
[- File dispatcher](https://gitlab.com/poomas/fidis)
<!-- [- Intercomm](https://gitlab.com/poomas/intercomm) -->
<!-- [subscriber](https://gitlab.com/poomas/subscriber) -->
